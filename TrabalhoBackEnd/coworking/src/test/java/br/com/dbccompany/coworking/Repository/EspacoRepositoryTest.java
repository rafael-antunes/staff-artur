package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.EspacoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class EspacoRepositoryTest {

    @Autowired
    private EspacoRepository repository;

    @Test
    public void salvarEspaco(){
        EspacoEntity espaco = new EspacoEntity();
        espaco.setValor(0.8);
        espaco.setNome("Praia");
        espaco.setQntPessoas(1000);
        repository.save(espaco);
        assertEquals(espaco.getNome(), repository.findByNome(espaco.getNome()).get().getNome());
    }
}
