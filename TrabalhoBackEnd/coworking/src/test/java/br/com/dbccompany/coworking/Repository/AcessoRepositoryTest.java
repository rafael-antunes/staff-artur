package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.*;
import org.aspectj.lang.annotation.After;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@DataJpaTest
public class AcessoRepositoryTest {

    @Autowired
    private AcessoRepository repository;

    @Autowired
    private SaldoClienteRepository saldoClienteRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    @Autowired
    private EspacoRepository espacoRepository;

    @Test
    public void salvarAcesso(){
        TipoContatoEntity tp = new TipoContatoEntity();
        TipoContatoEntity tp2 = new TipoContatoEntity();
        tp.setNome("telefone");
        tp2.setNome("email");
        tipoContatoRepository.save(tp);
        tipoContatoRepository.save(tp2);
        ContatoEntity cont = new ContatoEntity();
        ContatoEntity cont2 = new ContatoEntity();
        cont.setTipoContato(tp);
        cont2.setTipoContato(tp2);
        cont.setContato("51995544835");
        cont2.setContato("paula@gmail.com");
        List<ContatoEntity> contatos = new ArrayList<>();
        contatos.add(cont);
        contatos.add(cont2);
        ClienteEntity cliente = new ClienteEntity();
        cliente.setContatos(contatos);
        cliente.setNome("Paula");
        Character[] cpf = {'2', '5', '3', '2', '6', '4', '8', '2', '7', '1', '2', '0'};
        cliente.setCpf(cpf);
        cliente.setDataNascimento(LocalDate.parse("2002-10-04"));
        ClienteEntity clienteSalvo = clienteRepository.save(cliente);
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Berlin");
        espaco.setValor(12.0);
        espaco.setQntPessoas(500000);
        EspacoEntity espacoSalvo = espacoRepository.save(espaco);

        SaldoClienteEntity saldo = new SaldoClienteEntity();
        SaldoClienteId id = new SaldoClienteId();
        id.setId_cliente(clienteSalvo.getId());
        id.setId_espaco(espacoSalvo.getId());
        saldo.setId(id);
        saldo.setCliente(clienteSalvo);
        saldo.setEspaco(espacoSalvo);
        saldo.setTipoContratacao(TipoContratacaoEnum.HORA);
        saldo.setQuantidade(40);
        LocalDate dataAtual = LocalDate.now();
        saldo.setVencimento(dataAtual);
        saldoClienteRepository.save(saldo);

        AcessoEntity acesso = new AcessoEntity();
        acesso.setSaldoCliente(saldo);
        acesso.setEntrada(true);
        acesso.setExcessao(false);
        LocalDateTime dataAcesso = LocalDateTime.now();
        acesso.setData(dataAcesso);
        repository.save(acesso);
        assertEquals(acesso.getData(), repository.findById(1).get().getData());
    }
}
