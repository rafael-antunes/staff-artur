package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class TipoContatoRepositoryTest {

    @Autowired
    private TipoContatoRepository repository;

    @Test
    public void salvarTipoContato(){
        TipoContatoEntity tp = new TipoContatoEntity();
        tp.setNome("telefone");
        repository.save(tp);
        assertEquals(tp.getNome(), repository.findByNome(tp.getNome()).get().getNome());
    }

    @Test
    public void buscarTodosItens(){
        TipoContatoEntity tp1 = new TipoContatoEntity();
        TipoContatoEntity tp2 = new TipoContatoEntity();
        tp1.setNome("skype");
        tp2.setNome("instagram");
        repository.save(tp1);
        repository.save(tp2);
        List<TipoContatoEntity> expected = new ArrayList<>();
        expected.add(tp1);
        expected.add(tp2);
        assertEquals(expected, repository.findAll());
    }
}
