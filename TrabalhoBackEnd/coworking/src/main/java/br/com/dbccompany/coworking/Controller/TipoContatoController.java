package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.CoworkingApplication;
import br.com.dbccompany.coworking.DTO.ErroDTO;
import br.com.dbccompany.coworking.DTO.TipoContatoDTO;
import br.com.dbccompany.coworking.Exception.TipoContato.ErroAoSalvarTipoContato;
import br.com.dbccompany.coworking.Exception.TipoContato.TipoContatoNaoEncontrado;
import br.com.dbccompany.coworking.Service.TipoContatoService;
import br.com.dbccompany.coworking.Util.LogsGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Controller
@RequestMapping(value = "cw/tipoContato")
public class TipoContatoController {

    @Autowired
    private TipoContatoService service;

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<TipoContatoDTO> salvarTipoContato(@RequestBody TipoContatoDTO tipoContato){
        try{
            return new ResponseEntity<>(service.salvar(tipoContato.converter()), HttpStatus.CREATED);

        } catch(ErroAoSalvarTipoContato e){
            LogsGenerator.erro422(e);
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<TipoContatoDTO> editarTipoContato(@RequestBody TipoContatoDTO tipoContato,  @PathVariable Integer id ){
        try{
            return new ResponseEntity<>(service.editar(tipoContato.converter(), id), HttpStatus.CREATED);
        }catch(ErroAoSalvarTipoContato e){
            LogsGenerator.erro422(e);
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<TipoContatoDTO>> buscarTodos(){
        try{
            return new ResponseEntity<>(service.buscarTodos(), HttpStatus.OK);
        } catch (TipoContatoNaoEncontrado e){
            LogsGenerator.erro404(e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<TipoContatoDTO> buscarId(@PathVariable Integer id){
        try{
            return new ResponseEntity<>(service.buscarPorId(id), HttpStatus.OK);
        }catch(TipoContatoNaoEncontrado e){
            LogsGenerator.erro404(e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
