package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.Cliente_X_PacoteEntity;
import br.com.dbccompany.coworking.Entity.Espaco_X_PacoteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import br.com.dbccompany.coworking.Util.ConversorDinheiro;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class PacoteDTO {

    private Integer id;
    private String valor;
    private List<Espaco_X_PacoteDTO> espacoPacote;

    public PacoteDTO(PacoteEntity pacote){
        this.id = pacote.getId();
        this.valor = ConversorDinheiro.doubleParaString(pacote.getValor());
        this.espacoPacote = pacote.getEspacoPacotes() != null ? converterEPEntityParaDTO(pacote.getEspacoPacotes()) : null;
    }

    public PacoteDTO(){

    }

    public PacoteEntity converter(){
        PacoteEntity pacote = new PacoteEntity();
        pacote.setId(this.id);
        pacote.setValor(ConversorDinheiro.stringParaDouble(this.valor));
        pacote.setEspacoPacotes(this.espacoPacote != null ? converterEPDTOParaEntity(this.espacoPacote) : null);
        return pacote;
    }

    private List<Espaco_X_PacoteDTO> converterEPEntityParaDTO(List<Espaco_X_PacoteEntity> espacoPacote){
        List<Espaco_X_PacoteDTO> listDTO = new ArrayList<>();
        for(Espaco_X_PacoteEntity elem : espacoPacote){
            listDTO.add(new Espaco_X_PacoteDTO(elem));
        }
        return listDTO;
    }

    private List<Espaco_X_PacoteEntity> converterEPDTOParaEntity(List<Espaco_X_PacoteDTO> espacoPacote){
        List<Espaco_X_PacoteEntity> listEnt = new ArrayList<>();
        for(Espaco_X_PacoteDTO elem : espacoPacote){
            listEnt.add(elem.converter());
        }
        return listEnt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public List<Espaco_X_PacoteDTO> getEspacoPacote() {
        return espacoPacote;
    }

    public void setEspacoPacote(List<Espaco_X_PacoteDTO> espacoPacote) {
        this.espacoPacote = espacoPacote;
    }


}
