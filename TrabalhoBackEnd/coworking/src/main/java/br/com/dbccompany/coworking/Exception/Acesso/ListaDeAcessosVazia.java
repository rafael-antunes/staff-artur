package br.com.dbccompany.coworking.Exception.Acesso;

public class ListaDeAcessosVazia extends AcessoException {

    public ListaDeAcessosVazia(){
        super("Não há nenhum acesso registrado!");
    }
}
