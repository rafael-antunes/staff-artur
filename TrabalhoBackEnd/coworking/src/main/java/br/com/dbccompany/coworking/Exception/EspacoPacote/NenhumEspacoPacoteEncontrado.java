package br.com.dbccompany.coworking.Exception.EspacoPacote;

public class NenhumEspacoPacoteEncontrado extends EspacoPacoteException {
    public NenhumEspacoPacoteEncontrado(){
        super("Não foi possível encontrar nenhum relacionamento de espaço e pacote!");
    }
}
