package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.CoworkingApplication;
import br.com.dbccompany.coworking.DTO.ErroDTO;
import br.com.dbccompany.coworking.DTO.EspacoDTO;
import br.com.dbccompany.coworking.DTO.Espaco_X_PacoteDTO;
import br.com.dbccompany.coworking.Entity.TipoContratacaoEnum;
import br.com.dbccompany.coworking.Exception.EspacoPacote.ErroAoIncluirEspacoEmPacote;
import br.com.dbccompany.coworking.Exception.EspacoPacote.NenhumEspacoPacoteEncontrado;
import br.com.dbccompany.coworking.Service.EspacoService;
import br.com.dbccompany.coworking.Service.Espaco_X_PacoteService;
import br.com.dbccompany.coworking.Util.LogsGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Controller
@RequestMapping(value = "cw/espacoPacote")
public class Espaco_X_PacoteController {

    @Autowired
    private Espaco_X_PacoteService service;


    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<Espaco_X_PacoteDTO> salvar (@RequestBody Espaco_X_PacoteDTO espacoPacote){
        try{
            return new ResponseEntity<>(service.salvar(espacoPacote.converter()), HttpStatus.CREATED);
        }catch (ErroAoIncluirEspacoEmPacote e){
            LogsGenerator.erro422(e);
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<Espaco_X_PacoteDTO> editar(@RequestBody Espaco_X_PacoteDTO espacoPacote, @PathVariable Integer id){
        try{
            return new ResponseEntity<>(service.editar(espacoPacote.converter(), id), HttpStatus.CREATED);
        }catch (ErroAoIncluirEspacoEmPacote e){
            LogsGenerator.erro422(e);
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<Espaco_X_PacoteDTO>> buscarTodos(){
        try{
            return new ResponseEntity<>(service.buscarTodos(), HttpStatus.OK);
        }catch (NenhumEspacoPacoteEncontrado e){
            LogsGenerator.erro404(e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<Espaco_X_PacoteDTO> buscarId(@PathVariable Integer id){
        try{
            return new ResponseEntity<>(service.buscarPorId(id), HttpStatus.OK);
        }catch (NenhumEspacoPacoteEncontrado e){
            LogsGenerator.erro404(e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
