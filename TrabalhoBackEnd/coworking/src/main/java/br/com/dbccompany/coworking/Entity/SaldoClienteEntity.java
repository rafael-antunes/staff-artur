package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
public class SaldoClienteEntity {

    @EmbeddedId
    private SaldoClienteId id;

    @ManyToOne
    @MapsId("id_cliente")
    private ClienteEntity cliente;

    @ManyToOne
    @MapsId("id_espaco")
    private EspacoEntity espaco;

    @Column(nullable = false)
    private TipoContratacaoEnum tipoContratacao;

    @Column(nullable = false)
    private Integer quantidade;

    @Column(nullable = false)
    private LocalDate vencimento;

    @OneToMany(mappedBy = "saldoCliente")
    private List<AcessoEntity> acessos;

    public SaldoClienteId getId() {
        return id;
    }

    public void setId(SaldoClienteId id) {
        this.id = id;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public LocalDate getVencimento() {
        return vencimento;
    }

    public void setVencimento(LocalDate vencimento) {
        this.vencimento = vencimento;
    }

    public List<AcessoEntity> getAcessos() {
        return acessos;
    }

    public void setAcessos(List<AcessoEntity> acessos) {
        this.acessos = acessos;
    }
}
