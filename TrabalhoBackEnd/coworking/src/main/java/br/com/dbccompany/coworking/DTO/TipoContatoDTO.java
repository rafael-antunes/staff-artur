package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;

import java.util.ArrayList;
import java.util.List;

public class TipoContatoDTO {

    private Integer id;
    private String nome;

    public TipoContatoDTO(TipoContatoEntity tipoContato){
        this.id = tipoContato.getId();
        this.nome = tipoContato.getNome();
    }

    public TipoContatoDTO(){

    }

    public TipoContatoEntity converter(){
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setId(this.id);
        tipoContato.setNome(this.nome);
        return tipoContato;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
