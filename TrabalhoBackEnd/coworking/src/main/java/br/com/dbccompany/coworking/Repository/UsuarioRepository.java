package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.UsuarioEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UsuarioRepository extends CrudRepository<UsuarioEntity, Integer> {
    Optional<UsuarioEntity> findByLogin(String login);
}
