package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.DTO.EspacoDTO;
import br.com.dbccompany.coworking.DTO.SaldoClienteDTO;
import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Exception.SaldoCliente.ErroAoSalvarSaldo;
import br.com.dbccompany.coworking.Exception.SaldoCliente.PagamentoNaoFinalizado;
import br.com.dbccompany.coworking.Exception.SaldoCliente.SaldoNaoEncontrado;
import br.com.dbccompany.coworking.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SaldoClienteService {

    @Autowired
    private SaldoClienteRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public SaldoClienteDTO salvar(SaldoClienteEntity saldoCliente) throws ErroAoSalvarSaldo {
        try{
            return new SaldoClienteDTO(repository.save(saldoCliente));
        }catch (Exception e){
            throw new ErroAoSalvarSaldo();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public SaldoClienteDTO editar(SaldoClienteEntity saldoCliente, SaldoClienteId id) throws ErroAoSalvarSaldo {
        try{
            saldoCliente.setId(id);
            return salvar(saldoCliente);
        }catch (Exception e){
            throw new ErroAoSalvarSaldo();
        }
    }

    public List<SaldoClienteDTO> buscarTodos() throws SaldoNaoEncontrado {
        try{
            return converterEntityDTO(repository.findAll());
        }catch (Exception e){
            throw new SaldoNaoEncontrado();
        }
    }

    public SaldoClienteDTO buscarPorId(SaldoClienteId id) throws SaldoNaoEncontrado {
        Optional<SaldoClienteEntity> saldoCliente = repository.findById(id);
        if(saldoCliente.isPresent()){
            return new SaldoClienteDTO(saldoCliente.get());
        }
        throw new SaldoNaoEncontrado();
    }

    public SaldoClienteDTO buscarPorCliente(ClienteDTO cliente) throws SaldoNaoEncontrado {
        Optional<SaldoClienteEntity> saldoCliente = repository.findByCliente(cliente.converter());
        if(saldoCliente.isPresent()){
            return new SaldoClienteDTO(saldoCliente.get());
        }
        throw new SaldoNaoEncontrado();
    }

    public SaldoClienteDTO buscarPorEspaco(EspacoDTO espaco) throws SaldoNaoEncontrado {
        Optional<SaldoClienteEntity> saldoCliente = repository.findByEspaco(espaco.converter());
        if(saldoCliente.isPresent()){
            return new SaldoClienteDTO(saldoCliente.get());
        }
        throw new SaldoNaoEncontrado();
    }

    private List<SaldoClienteDTO> converterEntityDTO(List<SaldoClienteEntity> entities){
        List<SaldoClienteDTO> dtos = new ArrayList<>();
        for(SaldoClienteEntity entity : entities){
            dtos.add(new SaldoClienteDTO(entity));
        }
        return dtos;
    }

    @Transactional(rollbackFor = Exception.class)
    public List<SaldoClienteEntity> pagarClientePacote(Cliente_X_PacoteEntity clientePacote) throws PagamentoNaoFinalizado {
        List<SaldoClienteEntity> saldosCliente = new ArrayList<>();
        Integer quantidade = clientePacote.getQuantidade();
        ClienteEntity cliente = clientePacote.getCliente();
        List<Espaco_X_PacoteEntity> espacoPacotes = clientePacote.getPacote().getEspacoPacotes();
        for(Espaco_X_PacoteEntity espacoPacote : espacoPacotes){
            SaldoClienteId id = new SaldoClienteId();
            id.setId_cliente(cliente.getId());
            id.setId_espaco(espacoPacote.getEspaco().getId());
            if(saldoClienteExistente(id)){
                SaldoClienteEntity saldoCliente = repository.findById(id).get();
                saldoCliente.setQuantidade(saldoCliente.getQuantidade() + (quantidade * espacoPacote.getQuantidade()));
                saldoCliente.setVencimento(saldoCliente.getVencimento().plusDays((Integer)(quantidade * espacoPacote.getPrazo())));
                saldosCliente.add(repository.save(saldoCliente));
            }
            else{
                SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
                saldoCliente.setId(id);
                saldoCliente.setCliente(cliente);
                saldoCliente.setEspaco(espacoPacote.getEspaco());
                saldoCliente.setTipoContratacao(espacoPacote.getTipoContratacao());
                saldoCliente.setQuantidade(quantidade * espacoPacote.getQuantidade());
                saldoCliente.setVencimento(LocalDate.now().plusDays((Integer)(quantidade * espacoPacote.getPrazo())));
                saldosCliente.add(repository.save(saldoCliente));
            }
        }
        return saldosCliente;
    }

    @Transactional(rollbackFor = Exception.class)
    public SaldoClienteEntity pagarContratacao(ContratacaoEntity contratacao) throws PagamentoNaoFinalizado {
        SaldoClienteId id = new SaldoClienteId();
        id.setId_espaco(contratacao.getEspaco().getId());
        id.setId_cliente(contratacao.getCliente().getId());
        if(saldoClienteExistente(id)){
            SaldoClienteEntity saldoCliente = repository.findById(id).get();
            saldoCliente.setQuantidade(saldoCliente.getQuantidade() + contratacao.getQuantidade());
            saldoCliente.setVencimento(saldoCliente.getVencimento().plusDays((Integer)contratacao.getPrazo()));
            return repository.save(saldoCliente);

        } else{
            SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
            saldoCliente.setId(id);
            saldoCliente.setCliente(contratacao.getCliente());
            saldoCliente.setEspaco(contratacao.getEspaco());
            saldoCliente.setTipoContratacao(contratacao.getTipoContratacao());
            saldoCliente.setQuantidade(contratacao.getQuantidade());
            LocalDate vencimento = LocalDate.now().plusDays((Integer)contratacao.getPrazo());
            saldoCliente.setVencimento(vencimento);
            return repository.save(saldoCliente);
        }
    }

    private Boolean saldoClienteExistente(SaldoClienteId id){
        return repository.findById(id).isPresent();
    }
}
