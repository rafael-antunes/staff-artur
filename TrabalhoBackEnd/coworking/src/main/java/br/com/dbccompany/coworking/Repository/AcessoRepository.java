package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.AcessoEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface AcessoRepository extends CrudRepository<AcessoEntity, Integer> {

    List<AcessoEntity> findAll();
    Optional<AcessoEntity> findById(Integer id);
    List<AcessoEntity> findAllById(Iterable<Integer> ids);

    Optional<AcessoEntity> findBySaldoCliente(SaldoClienteEntity saldoCliente);
    List<AcessoEntity> findAllBySaldoCliente(SaldoClienteEntity saldoCliente);

    List<AcessoEntity> findAllBySaldoClienteAndEntradaOrderByData(SaldoClienteEntity saldoCliente, Boolean is_entrada);

    Optional<AcessoEntity> findByEntrada(Boolean entrada);
    List<AcessoEntity> findAllByEntrada(Boolean entrada);

    Optional<AcessoEntity> findByData(LocalDateTime data);
    List<AcessoEntity> findAllByData(LocalDateTime data);
}
