package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.CoworkingApplication;
import br.com.dbccompany.coworking.DTO.ContatoDTO;
import br.com.dbccompany.coworking.DTO.ErroDTO;
import br.com.dbccompany.coworking.DTO.TipoContatoDTO;
import br.com.dbccompany.coworking.Exception.Contato.ErroAoRegistrarContato;
import br.com.dbccompany.coworking.Exception.Contato.NenhumContatoEncontrado;
import br.com.dbccompany.coworking.Service.ContatoService;
import br.com.dbccompany.coworking.Util.LogsGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Controller
@RequestMapping(value = "cw/contato")
public class ContatoController {

    @Autowired
    private ContatoService service;

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<ContatoDTO> salvar (@RequestBody ContatoDTO contato){
        try{
            return new ResponseEntity<>(service.salvar(contato.converter()), HttpStatus.CREATED);
        }catch(ErroAoRegistrarContato e){
            LogsGenerator.erro422(e);
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<ContatoDTO> editar (@RequestBody ContatoDTO contato, @PathVariable Integer id){
        try{
            return new ResponseEntity<>(service.editar(contato.converter(), id), HttpStatus.CREATED);
        } catch (ErroAoRegistrarContato e){
            LogsGenerator.erro422(e);
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<ContatoDTO>> buscarTodos(){
        try{
            return new ResponseEntity<>(service.buscarTodos(), HttpStatus.OK);
        }catch (NenhumContatoEncontrado e){
            LogsGenerator.erro404(e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<ContatoDTO> buscarId(@RequestBody Integer id){
        try{
            return new ResponseEntity<>(service.buscarPorId(id), HttpStatus.OK);
        }catch(NenhumContatoEncontrado e){
            LogsGenerator.erro404(e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/buscarTipoContato")
    @ResponseBody
    public ResponseEntity<List<ContatoDTO>> buscarTipoContato(@RequestBody TipoContatoDTO tipoContato){
        try{
            return new ResponseEntity<>(service.buscarPorTipoContato(tipoContato.converter()), HttpStatus.OK);
        }catch (NenhumContatoEncontrado e){
            LogsGenerator.erro404(e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
