package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.CoworkingApplication;
import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.DTO.ErroDTO;
import br.com.dbccompany.coworking.DTO.EspacoDTO;
import br.com.dbccompany.coworking.DTO.SaldoClienteDTO;
import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteId;
import br.com.dbccompany.coworking.Exception.Cliente.ErroAoSalvarCliente;
import br.com.dbccompany.coworking.Exception.SaldoCliente.ErroAoSalvarSaldo;
import br.com.dbccompany.coworking.Exception.SaldoCliente.SaldoNaoEncontrado;
import br.com.dbccompany.coworking.Service.ClienteService;
import br.com.dbccompany.coworking.Service.SaldoClienteService;
import br.com.dbccompany.coworking.Util.LogsGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Controller
@RequestMapping(value = "cw/saldo")
public class SaldoClienteController {

    @Autowired
    private SaldoClienteService service;

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<SaldoClienteDTO> salvar(@RequestBody SaldoClienteDTO saldo){
        try{
            return new ResponseEntity<>(service.salvar(saldo.converter()), HttpStatus.CREATED);
        }catch (ErroAoSalvarSaldo e){
            LogsGenerator.erro422(e);
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<SaldoClienteDTO>> buscarTodos(){
        try{
            return new ResponseEntity<>(service.buscarTodos(), HttpStatus.OK);
        }catch (SaldoNaoEncontrado e){
            LogsGenerator.erro404(e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<SaldoClienteDTO> buscarId(@PathVariable SaldoClienteId id){
        try{
            return new ResponseEntity<>(service.buscarPorId(id), HttpStatus.OK);
        }catch (SaldoNaoEncontrado e){
            LogsGenerator.erro404(e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/cliente")
    @ResponseBody
    public ResponseEntity<SaldoClienteDTO> buscarId(@RequestBody ClienteDTO cliente){
        try{
            return new ResponseEntity<>(service.buscarPorCliente(cliente), HttpStatus.OK);
        }catch (SaldoNaoEncontrado e){
            LogsGenerator.erro404(e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/espaco")
    @ResponseBody
    public ResponseEntity<SaldoClienteDTO> buscarId(@RequestBody EspacoDTO espaco){
        try{
            return new ResponseEntity<>(service.buscarPorEspaco(espaco), HttpStatus.OK);
        }catch (SaldoNaoEncontrado e){
            LogsGenerator.erro404(e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
