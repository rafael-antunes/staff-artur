package br.com.dbccompany.coworking.Exception.Pagamento;

public class PagamentoNaoEncontrado extends PagamentoException {
    public PagamentoNaoEncontrado(){
        super("Nenhum pagamento encontrado!");
    }
}
