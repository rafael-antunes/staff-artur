package br.com.dbccompany.coworking.Exception.Contratacao;

import br.com.dbccompany.coworking.Exception.GeneralException;

public class ContratacaoException extends GeneralException {

    public ContratacaoException (String mensagem){
        super(mensagem);
    }
}
