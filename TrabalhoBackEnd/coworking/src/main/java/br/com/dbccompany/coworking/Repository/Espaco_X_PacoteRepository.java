package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Entity.Espaco_X_PacoteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import br.com.dbccompany.coworking.Entity.TipoContratacaoEnum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface Espaco_X_PacoteRepository extends CrudRepository<Espaco_X_PacoteEntity, Integer> {

    List<Espaco_X_PacoteEntity> findAll();
    Optional<Espaco_X_PacoteEntity> findById(Integer id);
    List<Espaco_X_PacoteEntity> findAllById(Iterable<Integer> ids);
}
