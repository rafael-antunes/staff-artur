package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.Cliente_X_PacoteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import br.com.dbccompany.coworking.Entity.PagamentoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface Cliente_X_PacoteRepository extends CrudRepository<Cliente_X_PacoteEntity, Integer> {

    List<Cliente_X_PacoteEntity> findAll();
    Optional<Cliente_X_PacoteEntity> findById(Integer id);
    List<Cliente_X_PacoteEntity> findAllById(Iterable<Integer> ids);

    Optional<Cliente_X_PacoteEntity> findByCliente(ClienteEntity cliente);
    List<Cliente_X_PacoteEntity> findAllByCliente(ClienteEntity cliente);

    Optional<Cliente_X_PacoteEntity> findByPacote(PacoteEntity pacote);
    List<Cliente_X_PacoteEntity> findAllByPacote(PacoteEntity pacote);

    Optional<Cliente_X_PacoteEntity> findByQuantidade(Integer quantidade);
    List<Cliente_X_PacoteEntity> findAllByQuantidade(Integer quantidade);


    Optional<Cliente_X_PacoteEntity> findByPagamentosIn(Iterable<PagamentoEntity> pagamentos);


}
