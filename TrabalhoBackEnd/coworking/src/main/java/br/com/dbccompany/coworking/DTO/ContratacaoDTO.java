package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.PagamentoEntity;
import br.com.dbccompany.coworking.Entity.TipoContratacaoEnum;

import java.util.ArrayList;
import java.util.List;

public class ContratacaoDTO {

    private Integer id;
    private EspacoDTO espaco;
    private ClienteDTO cliente;
    private TipoContratacaoEnum tipoContratacao;
    private Integer quantidade;
    private Double desconto;
    private Integer prazo;
    private String valor;

    public ContratacaoDTO(ContratacaoEntity contratacao){
        this.id = contratacao.getId();
        this.espaco = contratacao.getEspaco() != null ? new EspacoDTO(contratacao.getEspaco()) : null;
        this.cliente = contratacao.getCliente() != null ? new ClienteDTO(contratacao.getCliente()) : null;
        this.tipoContratacao = contratacao.getTipoContratacao();
        this.quantidade = contratacao.getQuantidade();
        this.desconto = contratacao.getDesconto();
        this.prazo = contratacao.getPrazo();
    }

    public ContratacaoDTO(){

    }

    public ContratacaoEntity converter(){
        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setId(this.id);
        contratacao.setEspaco(this.getEspaco() != null ? this.espaco.converter() : null);
        contratacao.setCliente(this.getCliente() != null ? this.cliente.converter() : null);
        contratacao.setTipoContratacao(this.tipoContratacao);
        contratacao.setQuantidade(this.quantidade);
        contratacao.setDesconto(this.desconto);
        contratacao.setPrazo(this.prazo);
        return contratacao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EspacoDTO getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoDTO espaco) {
        this.espaco = espaco;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
