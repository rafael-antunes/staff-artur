package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.PacoteDTO;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import br.com.dbccompany.coworking.Exception.Pacote.ErroAoRegistrarPacote;
import br.com.dbccompany.coworking.Exception.Pacote.PacoteNaoEncontrado;
import br.com.dbccompany.coworking.Repository.PacoteRepository;
import br.com.dbccompany.coworking.Util.ConversorDinheiro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Service
public class PacoteService {

    @Autowired
    private PacoteRepository repository;


    @Transactional(rollbackFor = Exception.class)
    public PacoteDTO salvar(PacoteEntity pacote) throws ErroAoRegistrarPacote {
        try{
            return new PacoteDTO(repository.save(pacote));
        }catch (Exception e){
            throw new ErroAoRegistrarPacote();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public PacoteDTO editar (PacoteEntity pacote, Integer id) throws ErroAoRegistrarPacote {
        try{
            pacote.setId(id);
            return salvar(pacote);
        }catch (Exception e){
            throw new ErroAoRegistrarPacote();
        }
    }

    private List<PacoteDTO> converterEntityDTO(List<PacoteEntity> pacotes) {
        List<PacoteDTO> dtos = new ArrayList<>();
        for(PacoteEntity pacote : pacotes){
            dtos.add(new PacoteDTO(pacote));
        }
        return dtos;
    }

    public List<PacoteDTO> buscarTodos() throws PacoteNaoEncontrado {
        try{
            return converterEntityDTO(repository.findAll());
        }catch (Exception e){
            throw new PacoteNaoEncontrado();
        }
    }

    public PacoteDTO buscarPorId(Integer id) throws PacoteNaoEncontrado {
        Optional<PacoteEntity> pacote = repository.findById(id);
        if(pacote.isPresent()){
            return new PacoteDTO(pacote.get());
        }
        throw new PacoteNaoEncontrado();
    }

}
