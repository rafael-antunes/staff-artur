package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ContatoRepository extends CrudRepository<ContatoEntity, Integer> {

    List<ContatoEntity> findAll();
    Optional<ContatoEntity> findById(Integer id);
    List<ContatoEntity> findAllById(Iterable<Integer> ids);

    Optional<ContatoEntity> findByTipoContato(TipoContatoEntity tipoContato);
    List<ContatoEntity> findAllByTipoContato(TipoContatoEntity tipoContato);

    Optional<ContatoEntity> findByContato(String contato);
    List<ContatoEntity> findAllByContato(String contato);

    List<ContatoEntity> findAllByCliente(ClienteEntity cliente);
}
