package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.AcessoEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteId;
import br.com.dbccompany.coworking.Entity.TipoContratacaoEnum;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class SaldoClienteDTO {

    private Integer id_cliente;
    private Integer id_espaco;
    private ClienteDTO cliente;
    private EspacoDTO espaco;
    private TipoContratacaoEnum tipoContratacao;
    private Integer quantidade;
    private LocalDate vencimento;

    public SaldoClienteDTO(SaldoClienteEntity saldoCliente){
        this.id_cliente = saldoCliente.getId().getId_cliente();
        this.id_espaco = saldoCliente.getId().getId_espaco();
        this.cliente = new ClienteDTO(saldoCliente.getCliente());
        this.espaco = new EspacoDTO(saldoCliente.getEspaco());
        this.tipoContratacao = saldoCliente.getTipoContratacao();
        this.quantidade = saldoCliente.getQuantidade();
        this.vencimento = saldoCliente.getVencimento();
    }

    public SaldoClienteDTO(){

    }

    public SaldoClienteEntity converter(){
        SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
        SaldoClienteId id = new SaldoClienteId();
        id.setId_cliente(this.id_cliente);
        id.setId_espaco(this.id_espaco);
        saldoCliente.setId(id);
        saldoCliente.setCliente(this.getCliente() != null ? this.cliente.converter() : null);
        saldoCliente.setEspaco(this.getEspaco() != null ? this.espaco.converter() : null);
        saldoCliente.setTipoContratacao(this.getTipoContratacao() != null ? this.tipoContratacao : null);
        saldoCliente.setQuantidade(this.getQuantidade() != null ? this.quantidade : null);
        saldoCliente.setVencimento(this.getVencimento() != null ? this.vencimento : null);
        return saldoCliente;
    }

    public Integer getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(Integer id_cliente) {
        this.id_cliente = id_cliente;
    }

    public Integer getId_espaco() {
        return id_espaco;
    }

    public void setId_espaco(Integer id_espaco) {
        this.id_espaco = id_espaco;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }

    public EspacoDTO getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoDTO espaco) {
        this.espaco = espaco;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public LocalDate getVencimento() {
        return vencimento;
    }

    public void setVencimento(LocalDate vencimento) {
        this.vencimento = vencimento;
    }
}
