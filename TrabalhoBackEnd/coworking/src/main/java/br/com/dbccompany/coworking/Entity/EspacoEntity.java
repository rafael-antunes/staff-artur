package br.com.dbccompany.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
public class EspacoEntity {

    @Id
    @SequenceGenerator(name = "ESPACO_SEQ", sequenceName = "ESPACO_SEQ")
    @GeneratedValue(generator = "ESPACO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(nullable = false, unique = true)
    private String nome;

    @Column(nullable = false)
    private Integer qtdPessoas;

    @Column(nullable = false)
    private Double valor;

    @OneToMany
    private List<ContratacaoEntity> contratacoes;

    @OneToMany(mappedBy = "espaco")
    private List<SaldoClienteEntity> saldoClientes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQntPessoas() {
        return qtdPessoas;
    }

    public void setQntPessoas(Integer qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public List<ContratacaoEntity> getContratacoes() {
        return contratacoes;
    }

    public void setContratacoes(List<ContratacaoEntity> contratacoes) {
        this.contratacoes = contratacoes;
    }

    public List<SaldoClienteEntity> getSaldoClientes() {
        return saldoClientes;
    }

    public void setSaldoClientes(List<SaldoClienteEntity> saldoClientes) {
        this.saldoClientes = saldoClientes;
    }
}