package br.com.dbccompany.coworking.Exception.ClientePacote;

public class NenhumRelacionamentoClientePacoteEncontrado extends ClientePacoteException{
    public NenhumRelacionamentoClientePacoteEncontrado(){
        super("Nenhum relacionamento Cliente-Pacote encontrado!");
    }
}
