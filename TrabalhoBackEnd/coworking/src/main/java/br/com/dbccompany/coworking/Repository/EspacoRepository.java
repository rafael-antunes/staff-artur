package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Entity.Espaco_X_PacoteEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface EspacoRepository extends CrudRepository<EspacoEntity, Integer> {

    List<EspacoEntity> findAll();
    Optional<EspacoEntity> findById(Integer id);
    List<EspacoEntity> findAllById(Iterable<Integer> ids);

    Optional<EspacoEntity> findByNome(String nome);

    Optional<EspacoEntity> findByValor(Double valor);
    List<EspacoEntity> findAllByValor(Double valor);

}
