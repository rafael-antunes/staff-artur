package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.*;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ContratacaoRepository extends CrudRepository<ContratacaoEntity, Integer> {

    List<ContratacaoEntity> findAll();
    Optional<ContratacaoEntity> findById(Integer id);
    List<ContratacaoEntity> findAllById(Iterable<Integer> ids);

    Optional<ContratacaoEntity> findByEspaco(EspacoEntity espaco);
    List<ContratacaoEntity> findAllByEspaco(EspacoEntity espaco);

    Optional<ContratacaoEntity> findByCliente(ClienteEntity cliente);
    List<ContratacaoEntity> findAllByCliente(ClienteEntity cliente);

    Optional<ContratacaoEntity> findByTipoContratacao(TipoContratacaoEnum tipoContratacao);
    List<ContratacaoEntity> findAllByTipoContratacao(TipoContratacaoEnum tipoContratacao);

    Optional<ContratacaoEntity> findByQuantidade(Integer quantidade);
    List<ContratacaoEntity> findAllByQuantidade(Integer quantidade);

    Optional<ContratacaoEntity> findByDesconto(Double desconto);
    List<ContratacaoEntity> findAllByDesconto(Double desconto);

    Optional<ContratacaoEntity> findByPrazo(Integer prazo);
    List<ContratacaoEntity> findAllByPrazo(Integer prazo);

    List<ContratacaoEntity> findByPagamentos(PagamentoEntity pagamento);

}
