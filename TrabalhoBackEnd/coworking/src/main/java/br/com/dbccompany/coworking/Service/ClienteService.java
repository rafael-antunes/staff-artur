package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Exception.Cliente.ClienteInformadoNaoEncontrado;
import br.com.dbccompany.coworking.Exception.Cliente.ErroAoSalvarCliente;
import br.com.dbccompany.coworking.Exception.Cliente.NenhumClienteRegistrado;
import br.com.dbccompany.coworking.Repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public ClienteDTO salvar (ClienteEntity cliente) throws ErroAoSalvarCliente {
        boolean temTelefone = false;
        boolean temEmail = false;
        List<ContatoEntity> contatos = cliente.getContatos();
        for(ContatoEntity contato : contatos){
            if(contato.getTipoContato().getId() == 1){
                temTelefone = true;
            }
            if(contato.getTipoContato().getId() == 2){
                temEmail = true;
            }
        }
        if(temTelefone && temEmail){
            return new ClienteDTO(repository.save(cliente));
        }
        throw new ErroAoSalvarCliente();
    }

    @Transactional(rollbackFor = Exception.class)
    public ClienteDTO editar(ClienteEntity cliente, Integer id) throws ErroAoSalvarCliente {
        try{
            cliente.setId(id);
            return salvar(cliente);
        }catch (Exception e){
            throw new ErroAoSalvarCliente();
        }
    }

    private List<ClienteDTO> converterEntityParaDTO(List<ClienteEntity> entities){
        List<ClienteDTO> dtos = new ArrayList<>();
        for(ClienteEntity elem : entities){
            dtos.add(new ClienteDTO(elem));
        }
        return dtos;
    }

    public List<ClienteDTO> buscarTodos() throws NenhumClienteRegistrado {
        try{
            return converterEntityParaDTO(repository.findAll());
        }catch (Exception e){
            throw new NenhumClienteRegistrado();
        }
    }

    public ClienteDTO buscarPorId(Integer id) throws ClienteInformadoNaoEncontrado {
        Optional<ClienteEntity> cliente = repository.findById(id);
        if(cliente.isPresent()){
            return new ClienteDTO(cliente.get());
        }
        throw new ClienteInformadoNaoEncontrado();
    }

}
