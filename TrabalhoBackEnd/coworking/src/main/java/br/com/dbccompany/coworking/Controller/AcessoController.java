package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.CoworkingApplication;
import br.com.dbccompany.coworking.DTO.AcessoDTO;
import br.com.dbccompany.coworking.DTO.ErroDTO;
import br.com.dbccompany.coworking.Exception.Acesso.*;
import br.com.dbccompany.coworking.Service.AcessoService;
import br.com.dbccompany.coworking.Util.LogsGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Controller
@RequestMapping(value = "cw/acesso")
public class AcessoController {

    @Autowired
    private AcessoService service;

    @PostMapping(value = "/entrar")
    @ResponseBody
    public ResponseEntity<AcessoDTO> entrar (@RequestBody AcessoDTO acesso){
        try{
            return new ResponseEntity<>(service.entrar(acesso.converter()), HttpStatus.CREATED);
        } catch(ErroAoRegistrarEntrada e){
            LogsGenerator.erro422(e);
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @PostMapping(value = "/sair")
    @ResponseBody
    public ResponseEntity<AcessoDTO> sair (@RequestBody AcessoDTO acesso){
        try{
            return new ResponseEntity<>(service.sair(acesso.converter()), HttpStatus.CREATED);
        } catch(ErroAoRegistrarSaida e){
            LogsGenerator.erro422(e);
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @PostMapping(value = "/entradaEspecial")
    @ResponseBody
    public ResponseEntity<AcessoDTO> entrarEspecial (@RequestBody AcessoDTO acesso){
        try{
            return new ResponseEntity<>(service.entradaEspecial(acesso.converter()), HttpStatus.CREATED);
        } catch (EntradaEspecialNaoAutorizada e){
            LogsGenerator.erro422(e);
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<AcessoDTO>> buscarTodos(){
        try{
            return new ResponseEntity<>(service.buscarTodos(), HttpStatus.OK);
        } catch(ListaDeAcessosVazia e){
            LogsGenerator.erro404(e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<AcessoDTO> buscarId(@PathVariable Integer id){
        try{
            return new ResponseEntity<>(service.buscarPorId(id), HttpStatus.OK);
        } catch(AcessoInexistente e){
            LogsGenerator.erro404(e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
