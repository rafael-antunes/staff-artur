package br.com.dbccompany.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
public class ClienteEntity {

    @Id
    @SequenceGenerator( name = "CLIENTE_SEQ", sequenceName = "CLIENTE_SEQ")
    @GeneratedValue( generator = "CLIENTE_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @Column(nullable = false)
    private String nome;

    @Column(nullable = false, unique = true)
    private Character[] cpf = new Character[11];

    @Column(nullable = false)
    private LocalDate dataNascimento;

    @OneToMany(cascade = CascadeType.ALL)
    private List<ContatoEntity> contatos;

    @OneToMany
    private List<ContratacaoEntity> contratacao;

    @OneToMany
    private List<Cliente_X_PacoteEntity> clientePacotes;

    @OneToMany
    private List<SaldoClienteEntity> saldosCliente;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Character[] getCpf() {
        return cpf;
    }

    public void setCpf(Character[] cpf) {
        this.cpf = cpf;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public List<ContatoEntity> getContatos() {
        return contatos;
    }

    public void setContatos(List<ContatoEntity> contatos) {
        this.contatos = contatos;
    }

    public List<ContratacaoEntity> getContratacao() {
        return contratacao;
    }

    public void setContratacao(List<ContratacaoEntity> contratacao) {
        this.contratacao = contratacao;
    }

    public List<Cliente_X_PacoteEntity> getClientePacotes() {
        return clientePacotes;
    }

    public void setClientePacotes(List<Cliente_X_PacoteEntity> clientePacotes) {
        this.clientePacotes = clientePacotes;
    }

    public List<SaldoClienteEntity> getSaldosCliente() {
        return saldosCliente;
    }

    public void setSaldosCliente(List<SaldoClienteEntity> saldosCliente) {
        this.saldosCliente = saldosCliente;
    }
}