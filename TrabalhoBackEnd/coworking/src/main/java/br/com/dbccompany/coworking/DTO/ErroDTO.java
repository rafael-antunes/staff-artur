package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Exception.GeneralException;

import java.time.LocalDateTime;

public class ErroDTO {

    private String id;
    private String tipo;
    private String data;
    private String codigo;
    private String descricao;

    public ErroDTO(GeneralException e, String tipo, String codigo ){
        this.data = LocalDateTime.now().toString();
        this.tipo = tipo;
        this.descricao = "Causa: " + e.getMensagem();
        this.codigo = codigo;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
