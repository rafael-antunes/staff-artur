package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Entity.Espaco_X_PacoteEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.Util.ConversorDinheiro;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class EspacoDTO {

    private Integer id;
    private String nome;
    private Integer qntPessoas;
    private String valor;

    public EspacoDTO(EspacoEntity espaco){
        this.id = espaco.getId();
        this.nome = espaco.getNome();
        this.qntPessoas = espaco.getQntPessoas();
        this.valor = ConversorDinheiro.doubleParaString(espaco.getValor());
    }

    public EspacoDTO(){

    }

    public EspacoEntity converter(){
        EspacoEntity espaco = new EspacoEntity();
        espaco.setId(this.id);
        espaco.setNome(this.nome);
        espaco.setQntPessoas(this.qntPessoas);
        espaco.setValor(this.getValor() != null ? ConversorDinheiro.stringParaDouble(this.valor) : null);
        return espaco;
    }


    private List<ContratacaoDTO> converterContratacaoEntityParaDTO(List<ContratacaoEntity> contratacoes){
        List<ContratacaoDTO> contrsDTO = new ArrayList<>();
        for(ContratacaoEntity contr : contratacoes){
            contrsDTO.add(new ContratacaoDTO(contr));
        }
        return contrsDTO;
    }

    private List<ContratacaoEntity> converterContratacaoDTOParaEntity(List<ContratacaoDTO> contratacoes){
        List<ContratacaoEntity> listaEnt = new ArrayList<>();
        for(ContratacaoDTO contr : contratacoes){
            listaEnt.add(contr.converter());
        }
        return listaEnt;
    }

    private List<Espaco_X_PacoteDTO> converterEspacoPacotesEntityParaDTO(List<Espaco_X_PacoteEntity> espacoPacotes){
        List<Espaco_X_PacoteDTO> espPctsDTO = new ArrayList<>();
        for(Espaco_X_PacoteEntity espPct : espacoPacotes){
            espPctsDTO.add(new Espaco_X_PacoteDTO(espPct));
        }
        return espPctsDTO;
    }

    private List<Espaco_X_PacoteEntity> converterEspacoPacotesDTOParaEntity(List<Espaco_X_PacoteDTO> espacoPacotes){
        List<Espaco_X_PacoteEntity> listEnt = new ArrayList<>();
        for(Espaco_X_PacoteDTO espPct : espacoPacotes){
            listEnt.add(espPct.converter());
        }
        return listEnt;
    }

    private List<SaldoClienteDTO> converterSaldoClienteEntityParaDTO(List<SaldoClienteEntity> saldoClientes){
        List<SaldoClienteDTO> listaDTO = new ArrayList<>();
        for(SaldoClienteEntity elemento : saldoClientes){
            listaDTO.add(new SaldoClienteDTO(elemento));
        }
        return listaDTO;
    }

    private List<SaldoClienteEntity> converterSaldoClienteDTOParaEntity(List<SaldoClienteDTO> saldoClientes){
        List<SaldoClienteEntity> listEnt = new ArrayList<>();
        for(SaldoClienteDTO elemento : saldoClientes){
            listEnt.add(elemento.converter());
        }
        return listEnt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQntPessoas() {
        return qntPessoas;
    }

    public void setQntPessoas(Integer qntPessoas) {
        this.qntPessoas = qntPessoas;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }


}
