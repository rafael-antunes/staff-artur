package br.com.dbccompany.coworking.Exception.Contato;

public class NenhumContatoEncontrado extends ContatoException{
    public NenhumContatoEncontrado(){
        super("Nenhum contato foi encontrado");
    }
}
