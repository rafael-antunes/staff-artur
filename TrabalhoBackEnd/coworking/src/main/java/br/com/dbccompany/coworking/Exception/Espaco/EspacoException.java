package br.com.dbccompany.coworking.Exception.Espaco;

import br.com.dbccompany.coworking.Exception.GeneralException;

public class EspacoException extends GeneralException {

    public EspacoException (String mensagem){
        super(mensagem);
    }
}
