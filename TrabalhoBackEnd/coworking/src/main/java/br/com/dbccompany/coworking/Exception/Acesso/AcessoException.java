package br.com.dbccompany.coworking.Exception.Acesso;

import br.com.dbccompany.coworking.Exception.GeneralException;

public class AcessoException extends GeneralException {

    public AcessoException(String mensagem){
        super(mensagem);
    }
}
