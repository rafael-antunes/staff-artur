package br.com.dbccompany.coworking.Exception.Pagamento;

import br.com.dbccompany.coworking.Exception.GeneralException;

public class PagamentoException extends GeneralException {

    public PagamentoException (String mensagem){
        super(mensagem);
    }
}
