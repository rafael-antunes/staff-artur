package br.com.dbccompany.coworking.Exception.Espaco;

public class NenhumEspacoEncontrado extends EspacoException{
    public NenhumEspacoEncontrado(){
        super("Nenhum espaço encontrado!");
    }
}
