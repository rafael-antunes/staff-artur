package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ContatoEntity;

public class ContatoDTO {

    private Integer id;
    private TipoContatoDTO tipoContato;
    private String contato;

    public ContatoDTO(ContatoEntity contato){
        this.id = contato.getId();
        this.tipoContato = new TipoContatoDTO(contato.getTipoContato());
        this.contato = contato.getContato();
    }

    public ContatoDTO(){

    }

    public ContatoEntity converter(){
        ContatoEntity contato = new ContatoEntity();
        contato.setId(this.id);
        contato.setTipoContato(this.getTipoContato() != null ? this.tipoContato.converter() : null);
        contato.setContato(this.contato);
        return contato;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContatoDTO getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContatoDTO tipoContato) {
        this.tipoContato = tipoContato;
    }

    public String getContato() {
        return contato;
    }

    public void setContato(String contato) {
        this.contato = contato;
    }

}
