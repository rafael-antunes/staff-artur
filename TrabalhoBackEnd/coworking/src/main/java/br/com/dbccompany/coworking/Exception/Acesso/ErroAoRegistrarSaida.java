package br.com.dbccompany.coworking.Exception.Acesso;

public class ErroAoRegistrarSaida extends AcessoException {

    public ErroAoRegistrarSaida(){
        super("Erro ao registrar a saída do cliente!");
    }
}
