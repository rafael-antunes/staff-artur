package br.com.dbccompany.logs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "logs")
public class LogController {

    @Autowired
    private LogService service;

    @PostMapping(value = "/salvar")
    @ResponseBody
    public LogDTO salvar (@RequestBody LogDTO log){
        return service.salvar(log.converter());
    }

    @GetMapping(value = "/")
    @ResponseBody
    public List<LogDTO> buscarTodos(){
        return service.buscarTodos();
    }

    @GetMapping(value = "/buscarTipo/{tipo}")
    @ResponseBody
    public List<LogDTO> buscarTodosPorTipo(@PathVariable String tipo){
        return service.buscarTodosPorTipo(tipo);
    }

    @GetMapping(value = "/buscarCodigo/{codigo}")
    @ResponseBody
    public List<LogDTO> buscarTodosPorCodigo(@PathVariable String codigo){
        return service.buscarTodosPorCodigo(codigo);
    }

}
