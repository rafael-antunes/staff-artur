package br.com.dbccompany.logs;

public class LogDTO {

    private String id;

    private String descricao;

    private String tipo;

    private String codigo;

    public LogDTO(LogEntity log) {
        this.id = log.getId();
        this.descricao = log.getDescricao();
        this.tipo = log.getTipo();
        this.codigo = log.getCodigo();
    }

    public LogDTO(){

    }

    public LogEntity converter(){
        LogEntity log = new LogEntity();
        log.setCodigo(this.codigo);
        log.setDescricao(this.descricao);
        log.setTipo(this.tipo);
        return log;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
}
