public class ElfoNoturno extends Elfo {
    
    public ElfoNoturno(String nome){
        super(nome);
        super.qntExperienciaPorAtaque = 3;
        super.qntDano = 15;
    }
}