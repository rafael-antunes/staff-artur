import java.util.Comparator;

public class OrdenarPorQtdFlecha implements Comparator<Elfo> {
    
    @Override
    public int compare(Elfo elfo1, Elfo elfo2) {
        return elfo2.getQntFlecha() - elfo1.getQntFlecha();
    }
}