import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoNoturnoTest{
    
    @Test
    public void elfoNoturnoNasceComUmArcoEDuasFlechas(){
        ElfoNoturno elfo = new ElfoNoturno("Dark Legolas");
        Item arco = new Item(1, "Arco");
        Item flecha = new Item(2, "Flecha");
        assertEquals(arco, elfo.getInventario().obter(0));
        assertEquals(flecha, elfo.getInventario().obter(1));
    }
    
    @Test
    public void elfoNoturnoAtiraFlechaGanha3XpPerde15Vida(){
        ElfoNoturno elfoNoturno = new ElfoNoturno("Dark Legolas");
        Anao anao = new Anao("Alvo");
        elfoNoturno.atirarFlecha(anao);
        assertEquals(85, elfoNoturno.getVida(), 0.1);
        assertEquals(3, elfoNoturno.getExperiencia());
    }
}

