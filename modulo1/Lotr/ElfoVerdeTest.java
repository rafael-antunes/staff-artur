

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest{
    
    @Test
    public void elfoVerdeNasceComArcoEFlecha(){
        ElfoVerde elfoVerde = new ElfoVerde("John");
        Item desejado1 = new Item (1, "Arco");
        Item desejado2 = new Item (2, "Flecha");
        assertEquals(desejado1, elfoVerde.getInventario().getItens().get(0));
        assertEquals(desejado2, elfoVerde.getInventario().getItens().get(1));
    }
    
    @Test
    public void elfoVerdeAtiraFlechaEGanha2DeExperiencia(){
        ElfoVerde elfoVerde = new ElfoVerde("John");
        Anao anao = new Anao("Gimli");
        elfoVerde.atirarFlecha(anao);
        assertEquals(2, elfoVerde.getExperiencia());
    }
    
    @Test
    public void elfoVerdeGanhaArcoDeVidro(){
        ElfoVerde elfoVerde = new ElfoVerde("John");
        Item arcoDeVidro = new Item (1, "Arco de Vidro");
        elfoVerde.ganharItem(arcoDeVidro);
        assertEquals(arcoDeVidro, elfoVerde.getInventario().getItens().get(2));
    }
    
    @Test
    public void elfoVerdeNaoGanhaEscudo(){
        ElfoVerde elfoVerde = new ElfoVerde("John");
        Item escudo = new Item (1, "Escudo");
        elfoVerde.ganharItem(escudo);
        assertEquals(2, elfoVerde.getInventario().getItens().size());
    }
}