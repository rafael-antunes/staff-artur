public class AnaoBarbaLonga extends Anao{
    
    public AnaoBarbaLonga(String nome){
        super(nome);
    }
    
    @Override
    public void sofrerDano(){
        int resultado = DadoD6.lancar();
        if (resultado < 5){
            super.sofrerDano();
        }
    }
}