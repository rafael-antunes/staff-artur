import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoDaLuzTest{
    
    @Test
    public void elfoDaLuzNasceComArcoFlechasEEspada(){
        ElfoDaLuz elfoDL = new ElfoDaLuz("Rudolff");
        Item arco = new Item(1, "Arco");
        Item flecha = new Item(2, "Flecha");
        Item espada =new Item (1, "Espada de Galvorn");
        assertEquals(arco, elfoDL.getInventario().obter(0));
        assertEquals(flecha, elfoDL.getInventario().obter(1));
        assertEquals(espada, elfoDL.getInventario().obter(2));
    }
    
    @Test
    public void elfoAtacaAnaoComEspadaEPerde21DeVidaDepoisAtacaDeNovoEGanha10(){
        ElfoDaLuz elfoDL = new ElfoDaLuz("Light Legolas");
        Anao anao = new Anao("Jefferson");
        assertEquals(100.0, elfoDL.getVida(), 0.1);
        elfoDL.ataqueEspada(anao);
        assertEquals(79.0, elfoDL.getVida(), 0.1);
        elfoDL.ataqueEspada(anao);
        assertEquals(89.0, elfoDL.getVida(), 0.1);
        assertEquals(90.0, anao.getVida(), 0.1);
    }
}