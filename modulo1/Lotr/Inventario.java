import java.util.*;

public class Inventario{
    
    private ArrayList <Item> itens;
    private TipoOrdenacao tipoOrdenacao;
    
    public Inventario(){
        this.itens = new ArrayList<>();
    }
    
    
    public ArrayList<Item> getItens (){
        return this.itens;
    }
    
    public void adicionar (Item item){
        this.itens.add(item);    
    }
    
    private boolean validarSePosicao(int posicao){
        return posicao >= this.itens.size();
    }
    
    public Item obter(int posicao){
        if (validarSePosicao(posicao)){
            return null;
        }
        return this.itens.get(posicao);
    }
    
    public Item buscar(String descricao){
        for (Item item : this.itens){
            if (item.getDescricao().equals(descricao)){
                return item;
            }
        }
        return null;
    } 
    
    public void remover (int posicao){
        if (!this.validarSePosicao(posicao)){
            this.itens.remove(posicao);
        }
        
    }
    
    public void remover (Item item){
        this.itens.remove(item);
    }
    
    public String getDescricoesItens(){
        StringBuilder descricoes = new StringBuilder();
        boolean primeiroItem = true;
        for (Item item : this.itens){
            String descricao = item.getDescricao();
            if (primeiroItem){
                descricoes.append(descricao);
                primeiroItem = false;
            } else {
                descricoes.append(", ");
                descricoes.append(descricao);
            }
        }
        return descricoes.toString();
    }
    
    public Item getItemMaiorQuantidade(){
        Item maiorQuantidade = this.itens.get(0);
        int quantidade = 0;
        for (Item item : this.itens){
            if (item.getQuantidade() > maiorQuantidade.getQuantidade()){
                maiorQuantidade = item;
            }
        }
        return maiorQuantidade;
    }
    
    public ArrayList<Item> inverter(){
        ArrayList<Item> invertido = new ArrayList<>();
        for ( int i = this.itens.size() - 1; i >= 0; i-- ){
            Item adicao = this.itens.get(i);
            invertido.add(adicao);
        }
        return invertido;
    }
    
    public void ordenarItens(){
        this.ordenarItens(TipoOrdenacao.ASC);
    }
    
    public void ordenarItens(TipoOrdenacao ordenacao){
        for ( int i = 0; i < this.itens.size(); i++ ){
            for (int j = 0; j < this.itens.size() - 1; j++){
                Item atual = this.itens.get(j);
                Item proximo = this.itens.get(j + 1);
                boolean deveTrocar = ordenacao == TipoOrdenacao.ASC ?
                (atual.getQuantidade() > proximo.getQuantidade()) :
                (atual.getQuantidade() < proximo.getQuantidade());
                if (deveTrocar){
                    Item itemTrocado = atual;
                    this.itens.set(j, proximo);
                    this.itens.set(j + 1, itemTrocado);
                } 
            }
        }
    }
}
