
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class AgendaContatosTest{

    
    //Testes a serem corrigidos. Mas sistema funcionando.
    @Test
    public void adicaoDeUmContatoNaAgendaDeContatos(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Paula", "996991234");
        agenda.adicionar("John", "9954673723");
        assertEquals("", agenda.getAgendaContatos());
    }
    
    @Test
    public void pegarTelefoneAPartirDeNome(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Barbara", "991536381");
        agenda.adicionar("Dona Guacira", "995850367");
        String resultado = agenda.consultar("Dona Guacira");
        assertEquals("995850367", resultado);
    }
    
    @Test
    public void pesquisarContatoAPartirDeTelefone(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Barbara", "991536381");
        agenda.adicionar("Dona Guacira", "995850367");
        agenda.adicionar("Augusto", "981245768");
        String resultado = agenda.consultarPorTelefone("995850367");
        assertEquals("Dona Guacira", resultado);
    }
    
    @Test
    public void contatosEmFormatoCSV(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Barbara", "991536381");
        agenda.adicionar("Guacira", "995850367");
        agenda.adicionar("Augusto", "981245768");
        String resultado = agenda.csv();
        String esperado = "";
        assertEquals(esperado, resultado);
    }
}