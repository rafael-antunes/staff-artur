import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Rotas from './Pages/Rotas'

ReactDOM.render( <Rotas />, document.getElementById( 'root' ) );