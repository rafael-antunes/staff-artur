import React from 'react';
import Usuario from '../../Models/Usuario';
import { Link } from 'react-router-dom';
import AuthenticationApi from '../../API/AuthenticationApi';

export default class Cadastro extends React.Component{
  constructor( props ){
    super( props );
    this.api = new AuthenticationApi();
    this.state ={
      nome: '',
      email: '',
      login: '',
      senha: '',
      usuarioSalvo: '',
      mensagem: ''
    }
  }
    
  cadastrarUsuario(){
     const novoUsuario = new Usuario( this.state );
     if( !Object.values( novoUsuario ).includes( '' ) ){
      this.api.cadastrar( novoUsuario ).then( user => {
        this.setState( state => {
          return{...state, usuarioSalvo: user } } )
      } );
     } else{
       this.camposInvalidos();
     }
  }

  camposInvalidos(){
    this.setState( state => {
      return{
        ...state,
        mensagem: 'Preencha todos os campos!'
      }
    } );
    console.log( this.state )
    this.removerMensagem();
  }

  removerMensagem(){
    setTimeout( () => {
      this.setState( state => {
        return{
          ...state,
          mensagem: ''
        }
      } )
    }, 3000 );
  }

  

  render(){
    return(
      <React.Fragment>
        <div>
          <input type='text' placeholder='Nome' onChange={ evt => this.setState( state => { return{ ...state, nome: evt.target.value } } ) } />
          <input type='text' placeholder='Email' onChange={ evt => this.setState( state => { return{ ...state, email: evt.target.value } } ) } />
          <input type='text' placeholder='Login' onChange={ evt => this.setState( state => { return{ ...state, login: evt.target.value } } ) } />
          <input type='text' placeholder='Senha' onChange={ evt => this.setState( state => { return{ ...state, senha: evt.target.value } } ) } />
          <button onClick={ this.cadastrarUsuario.bind( this ) } >Cadastrar</button>
          <p>{ this.state.mensagem }</p>
        </div>
        
        { this.state.usuarioSalvo && (
          <div>
            <h2>Confira seus dados</h2>
            <p> Nome do usuário: { this.state.usuarioSalvo.nome }</p>
            <p>E-mail: { this.state.usuarioSalvo.email }</p>
            <p>Login: { this.state.usuarioSalvo.login }</p>
            <p>Senha: { this.state.usuarioSalvo.senha }</p>
          </div>
        ) }        
        <Link to='/' >Ir para Login</Link>
      </React.Fragment>
    )
  }
}