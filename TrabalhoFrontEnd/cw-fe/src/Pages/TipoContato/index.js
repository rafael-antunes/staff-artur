import React from "react";

import 'antd/dist/antd.css';

import ApiBuscar from "../../API/ApiBuscar";
import ApiSalvar from "../../API/ApiSalvar";
import TipoContatoModel from "../../Models/TipoContatoModel";

export default class TipoContato extends React.Component{
  constructor( props ){
    super( props );
    this.salvarTipoContato = this.salvarTipoContato.bind( this );
    this.apiSalvar = new ApiSalvar();
    this.apiBuscar = new ApiBuscar();
    this.state = {
      todos: '',
      idAEditar: '',
      tipoContatoEditado: ''
    }
  }

  componentDidMount(){
    this.buscarTodos();
  }


  salvarTipoContato( nome ){
    if( nome !== '' ){
      const novoTipoContato = new TipoContatoModel( nome );
      this.apiSalvar.salvarTipoContato( novoTipoContato ).then( () =>
        this.buscarTodos() );
    }
  }

  editarTipoContato(){
     if( this.state.idAEditar !== null && this.state.tipoContatoEditado !== null ){
       const tipoContatoEditado = new TipoContatoModel( this.state.tipoContatoEditado );
       this.apiSalvar.editarTipoContato( tipoContatoEditado, this.state.idAEditar ).then( () =>
         this.buscarTodos() );
     }
  }

  buscarTodos(){
    this.apiBuscar.buscarTiposContato().then( tiposContato => {
      this.setState( state => { return{ ...state, todos: tiposContato } } );
    } )
  }

  render(){
    return(
      <React.Fragment>
        <p>Salvar</p>
        <input type='text' placeholder='Novo Tipo Contato' onBlur={ evt => { this.salvarTipoContato( evt.target.value ) } } />
        <p>Editar</p>
        <input type='number' placeholder='Id' onBlur={ evt => this.setState( state => { return{ ...state, idAEditar: evt.target.value } } ) } />
        <input type='text' placeholder='Editar' onBlur={ evt => this.setState( state => { return{ ...state, tipoContatoEditado: evt.target.value } } ) } />
        <button onClick={ () => this.editarTipoContato() } >Editar</button>
        { this.state.tipoContatoSalvo ? (
          <div>
            <p>Id: { this.state.tipoContatoSalvo.id }</p>
            <p>Nome: { this.state.tipoContatoSalvo.nome }</p>
          </div>
        ) : '' }
        { !this.state.todos ?
          <h3>Aguarde...</h3> :
         
           this.state.todos.map( tp => {
             return(
              <p>Id: { tp.id } / Nome: { tp.nome }</p>
             )  
           })
         
        }
      </React.Fragment>
    )
  }
}