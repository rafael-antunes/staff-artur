import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import { RotasPrivadas } from './RotasPrivadas';

import Cadastro from '../Cadastro';
import Login from '../Login';
import App from '../../App'
import TipoContato from '../TipoContato';


export default class Rotas extends React.Component{
  render(){
    return(
      <Router>
        <Route path='/cadastro' exact component={ Cadastro } />
        <Route path='/' exact component={ Login }/>
        <RotasPrivadas path='/main' exact component={ App } />
        <RotasPrivadas path='/tipo-contato' exact component={ TipoContato } />
      </Router>
    )
  }
}