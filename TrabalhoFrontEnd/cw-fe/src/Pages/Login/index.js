import React from "react";
import { Link } from 'react-router-dom';

import { Form, Input, Button, Checkbox } from 'antd';
import 'antd/dist/antd.css';

import AuthenticationApi from "../../API/AuthenticationApi";
import Usuario from "../../Models/Usuario";

export default class Login extends React.Component{
  constructor( props ){
    super( props );
    this.api = new AuthenticationApi();
    this.state = {
      login: '',
      senha: '',
      mensagem: '',
      exibirAcesso: false
    }
  }

  fazerLogin(){
    const usuario = new Usuario( this.state );
    this.api.login( usuario ).then( res => {
      localStorage.setItem( 'token', res.headers.authorization );
      this.autenticacaoBemSucessida();
      setTimeout( () => {
        this.setState( state => {
          return{
            ...state,
            exibirAcesso: true
          }
        } )
      }, 2000 )    
    } ).catch ( () => {
      this.usuarioInvalido();
    } );
  }

  autenticacaoBemSucessida(){
    this.setState( state => {
      return{
        ...state,
        mensagem: "Usuário autenticado! Aguarde..."
      }
    } );
    this.removerMensagem();
  }

  usuarioInvalido(){
    this.setState( state => {
      return{
        ...state,
        mensagem: "Usuário inválido!"
      }
    } );
    this.removerMensagem();
  }

  removerMensagem(){
    setTimeout( () => {
      this.setState( state => {
        return{
          ...state,
          mensagem: ''
        }
      } )
    }, 5000 );
  }

  render(){
    return(
      <React.Fragment>
        <input placeholder='Login' onChange={ evt => this.setState( state => { return{ ...state, login: evt.target.value } } ) } />
        <input placeholder='Senha' onChange={ evt => this.setState( state => { return{ ...state, senha: evt.target.value } } ) } />
        <button onClick={ this.fazerLogin.bind( this ) } >Login</button>
        <Link to='/cadastro' >Realizar Cadastro</Link>
        <h3>{ this.state.mensagem }</h3>
        { this.state.exibirAcesso ? <Link to='/main' >Acessar Aplicação</Link> : '' }
      </React.Fragment>
    )
  }
}