import axios from "axios";

export default class ApiSalvar{
  constructor(){
    this.url = 'http://localhost:8080/cw';
    this.token = localStorage.getItem( 'token' );
    this.config = { headers: { Authorization: this.token } };
  }

  salvarTipoContato( tipoContato ){
    return axios.post( `${ this.url }/tipoContato/salvar`, tipoContato, this.config ).then( res => res.data );
  };

  editarTipoContato( tipoContato, id ){
    return axios.put( `${ this.url }/tipoContato/editar/${ id }`, tipoContato, this.config )
  }

  // salvarCliente( { nome, cpf, dataNascimento, contatos } );

  // salvarEspaco( { nome, qntPessoas, valor } );

  // salvarPacote( { valor, espacoPacote, quantidade } );

  // salvarClientePacote( { cliente, pacote, quantidade } );

  // salvarContratacao( { espaco, cliente, tipoContratacao, quantidade, prazo } );

  // salvarPagamento( { contratacaoOuClientePacote, tipoPagamento } );

  // salvarEntrada( { saldoCliente } );

  // salvarSaida( { saldoCliente } );
}