import axios from "axios";

export default class ApiBuscar{
  constructor(){
    this.url = 'http://localhost:8080/cw';
    this.token = localStorage.getItem( 'token' );
    this.config = { headers: { Authorization: this.token } };
  }

  buscarTiposContato(){
    return axios.get( `${ this.url }/tipoContato/`, this.config ).then( res => res.data );
  }
}