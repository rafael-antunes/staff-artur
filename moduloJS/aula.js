console.log("Chegou até aqui!");

// VAR
//var nome = "Marcos";
//var nome = "Marcos H";
//console.log(nome);



let nome1 = "Marcos"; //Não pode ser declarado mais de uma vez.

{
    let nome1 = "Marcos";
    nome1 = "Marcos H";
    console.log(nome1);
}

console.log(nome1);


//CONST

//const nome2 = "Marcos - Constante";
const pessoa = {
    nome: "Marcos - Constante"
};

Object.freeze(pessoa); //Transforma a constante em uma constante de "verdade".

pessoa.nome = "Marcos - Constante";

console.log(pessoa.nome);


// ESPETACULAR

console.log("Nossa que incrível!");

let soma = 1 + 2;
let soma1 = "1" + 2 + 3;
let soma2 = 1 + "2" + 3;
let soma3 = 1 + 2 + "3";

console.log(soma);
console.log(soma1);
console.log(soma2);
console.log(soma3);

//FUNÇÕES

let nome = "Marcos";
let idade = 31;
let semestre = 5;
let notas = [10.0, 3, 5, 9];



aluno.status = aprovadoOuReprovado(aluno.notas);
console.log (aluno);



function criarAluno(nome, idade, semestre, notas = []) {
    const aluno = {
        nome: nome,
        idade: idade,
        semestre: semestre,
        notas: notas
    };
    console.log(aluno);

    //factory --> Design Pattern
    function aprovadoOuReprovado( notas ) {
        if (notas.length === 0){
            return "Sem notas";
        }
    
        let somatoria = 0
        for (let i = 0; i < notas.length; i++) {
            somatoria += notas[i];
        }
    
        return (somatoria / notas.length) > 7.0 ? "Aprovado" : "Reprovado";
    }
    aluno.status = aprovadoOuReprovado( notas );
    console.log (aluno);
    return aluno;    
}

criarAluno(nome, idade, semestre, notas);
criarAluno(nome, idade, semestre);

let alunoExterno = criarAluno(nome, idade, semestre, notas);
console.log(alunoExterno);




