/* eslint-disable no-unused-vars */

const erro = ( mensagem, tempo ) => {
  const campoErro = document.getElementById( 'mensagem-erro' );
  campoErro.innerText = mensagem;
  setTimeout( () => {
    campoErro.innerText = '';
  }, tempo );
}

const idInvalido = ( pokemonId ) => ( pokemonId > 898 && pokemonId < 10001 ? erro( 'Todos os IDs entre 898 e 10001 são inválidos. \n Tente novamente!', 8000 ) : erro( 'Digite um ID válido!', 3000 ) );


const mesmoId = () => {
  erro( 'Pokemon buscado é o mesmo \n disposto na tela!', 5000 );
}

const registroCheio = () => {
  erro( 'Todos os Pokemons possíveis foram sorteados. \n O registro foi deletado e reiniciado.', 8000 );
  localStorage.setItem( 'registro', '' );
}
