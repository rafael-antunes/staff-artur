const primeiraLetraMaiuscula = ( elemento ) => {
  const arr = elemento.split( ' ' );
  for ( let i = 0; i < arr.length; i += 1 ) {
    arr[i] = arr[i].charAt( 0 ).toUpperCase() + arr[i].slice( 1 );
  }
  return arr.join( ' ' );
}

const tiposEmTabela = ( tipos ) => {
  const tiposHead = document.getElementById( 'tipos-head' );
  tiposHead.innerText = '';
  tiposHead.insertRow().insertCell().innerText = 'Tipos';
  const tiposBody = document.getElementById( 'tipos-body' );
  tiposBody.innerText = '';
  for ( let i = 0; i < tipos.length; i += 1 ) {
    const tr = tiposBody.insertRow();
    const tipo = tr.insertCell();
    tipo.innerText = primeiraLetraMaiuscula( tipos[i].type.name );
  }
}

const estatisticasEmTabela = ( estatisticas ) => {
  const statsHead = document.getElementById( 'stats-head' );
  statsHead.innerText = '';
  const statsRow = statsHead.insertRow();
  statsRow.insertCell().innerText = 'Atributo';
  statsRow.insertCell().innerText = 'Base';
  statsRow.insertCell().innerText = 'Esforço';

  const statsBody = document.getElementById( 'stats-body' );
  statsBody.innerText = '';
  for ( let i = 0; i < estatisticas.length; i += 1 ) {
    const tr = statsBody.insertRow();

    const tdStat = tr.insertCell();
    const tdBase = tr.insertCell();
    const tdEffort = tr.insertCell();

    tdStat.innerText = primeiraLetraMaiuscula( estatisticas[i].stat.name.replace( '-', ' ' ) );
    tdBase.innerText = estatisticas[i].base_stat;
    tdEffort.innerText = estatisticas[i].effort;
  }
}

const renderizar = ( pokemon ) => { // eslint-disable-line no-unused-vars
  const dadosPokemon = document.getElementById( 'dadosPokemon' );

  const nome = dadosPokemon.querySelector( '.nome' );
  nome.innerHTML = primeiraLetraMaiuscula( pokemon.nome.replaceAll( '-', ' ' ) );

  const id = dadosPokemon.querySelector( '.id' );
  id.innerHTML = pokemon.id;

  const altura = dadosPokemon.querySelector( '.altura' );
  altura.innerHTML = pokemon.altura;

  const peso = dadosPokemon.querySelector( '.peso' );
  peso.innerHTML = pokemon.peso;
  const imagem = dadosPokemon.querySelector( '.thumb' );
  imagem.src = pokemon.imagem;

  tiposEmTabela( pokemon.tipos );

  estatisticasEmTabela( pokemon.estatisticas );
}
