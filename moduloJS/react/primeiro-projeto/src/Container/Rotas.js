import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import App from './App';
import ListaAvaliacoes from './Avaliacoes';
import DetalhesEpisodio from './DetalhesEpisodio';
import TodosEpisodios from './TodosEpisodios';

import { RotasPrivadas } from '../Components/RotasPrivadas';
import Formulario from './Formulario';

export default class Rotas extends Component {
  render(){
    return (
      <Router>
        <Route path="/" exact component={ App } />
        <Route path="/avaliacoes" exact component={ ListaAvaliacoes } />
        <Route path='/detalhes-episodio' exact component={ DetalhesEpisodio } />
        <RotasPrivadas path='/todos-episodios' exact component={ TodosEpisodios } />
        <Route path='/formulario' component={ Formulario } />
      </Router>
    );
  }
}