import Episodio from "./Episodio";

function sortear( min, max ) {
  min = Math.ceil( min );
  max = Math.floor( max );
  return Math.floor( Math.random() * ( max - min ) ) + min;
}

export default class ListaEpisodios {
  constructor( episodioAPI ) {
    this.todos = episodioAPI.map( episodio => new Episodio( episodio ) );
  }

  get episodioAleatorio() {
    const indice = sortear( 0, this.todos.length );
    return this.todos[ indice ];
  }

  get avaliados() {
    return this.ordenarPorTemporadaEEpisodio( this.todos.filter( e => e.nota ) );
  }

  get todosEpisodios(){
    return this.ordenarPorTemporadaEEpisodio( this.todos );
  }

  ordenarPorTemporadaEEpisodio( array ){
    return array.sort( ( a, b ) => {
      return a.temporada < b.temporada ? -1 : a.temporada > b.temporada ? 1 : a.ordem < b.ordem ? -1 : a.ordem > b.ordem ? 1 : 0;
    }  );
  }

  ordenarPorDuracaoCrescente(){ 
    return this.todos.sort( ( a, b ) => {
      return a.duracao < b.duracao ? -1 : a.duracao > b.duracao ? 1 : -1 } )    
  }

  ordenarPorDuracaoDecrescente(){
    return this.todos.sort( ( a, b ) => {
    return a.duracao > b.duracao ? -1 : a.duracao < b.duracao ? 1 : -1 } );
  }

  ordenarPorEstreiaCrescente(){
    return this.todos.sort( (a, b) => {
      const dataA = a.quebrarDataEstreia().map( e => parseInt(e) );
      const dataB = b.quebrarDataEstreia().map( e => parseInt(e) );
      return dataA[0] > dataB[0] ? -1 : dataA[0] < dataB[0] ? 1 :
            dataA[1] > dataB[1] ? -1 : dataA[1] < dataB[1] ? 1 :
            dataA[2] > dataB[2] ? -1 : dataA[2] < dataB[2] ? 1 : 0
    })
  }

  ordenarPorEstreiaDecrescente(){
    return this.todos.sort( (a, b) => {
      const dataA = a.quebrarDataEstreia().map( e => parseInt(e) );
      const dataB = b.quebrarDataEstreia().map( e => parseInt(e) );
      return dataA[0] < dataB[0] ? -1 : dataA[0] > dataB[0] ? 1 :
            dataA[1] < dataB[1] ? -1 : dataA[1] > dataB[1] ? 1 :
            dataA[2] < dataB[2] ? -1 : dataA[2] > dataB[2] ? 1 : 0
    })
  }
}