import axios from 'axios';

const endereco = 'http://localhost:9000/api';

export default class EpisodioAPI {
  buscar(){
    return axios.get( `${ endereco }/episodios` ).then( e => e.data );
  }

  buscarNotas(){
    return axios.get( `${ endereco }/notas` ).then( n => n.data );
  }

  buscarDetalhes(){
    return axios.get( `${ endereco }/detalhes` ).then( e => e.data );
  }

  buscarDetalhesPorId( id ){
    return axios.get( `${ endereco }/episodios/${ id }/detalhes` ).then( e => e.data );
  }

  registrarNota( { nota, episodioId } ){
    return axios.post( `${ endereco }/notas`, { nota, episodioId } );
  }

}