import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import App from './App';
import ListaAvaliacoes from './Avaliacoes';
import DetalhesEpisodio from './DetalhesEpisodio';
import TodosEpisodios from './TodosEpisodios';

export default class Rotas extends Component {
  render(){
    return (
      <Router>
        <Route path="/" exact component={ App } />
        <Route path="/avaliacoes" component={ ListaAvaliacoes } />
        <Route path='/detalhes-episodio' component={ DetalhesEpisodio } />
        <Route path='/todos-episodios' component={ TodosEpisodios } />
      </Router>
    );
  }
}