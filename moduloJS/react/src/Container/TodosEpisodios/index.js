import React from 'react';
import Episodio from '../../models/Episodio';
import { Link, link } from 'react-router-dom'
import BotaoUi from '../../Components/BotaoUi';

import EpisodioAPI from '../../models/EpisodioAPI';

export default class TodosEpisodios extends React.Component{
  constructor(props){
    super(props);
    this.api = new EpisodioAPI();
    this.state = {
      listaEpisodios: this.props.location.state.listaEpisodios,
      detalhes: '',
      estreiaCrescente: true,
      duracaoCrescente: true      
    }
  }

  componentDidMount(){
    this.api.buscarDetalhes().then( detalhes => {
      this.setState( state => { return{ ...state, detalhes: detalhes } } );
    })
  }

  ordenarPorDuracao(){
    let flag = false;
    if( this.state.duracaoCrescente === true ){
      this.state.listaEpisodios.ordenarPorDuracaoCrescente();
    } else{
      this.state.listaEpisodios.ordenarPorDuracaoDecrescente();
      flag = true;
    }
    this.setState( state => { return{ ...state, listaEpisodios: this.state.listaEpisodios, duracaoCrescente: flag } } );
  }

  ordenarPorDataDeEstreia(){
    this.atribuirDatasEstreia();
    let flag = false;
    if( this.state.estreiaCrescente === true ) {
      this.state.listaEpisodios.ordenarPorEstreiaCrescente();
    } else{ 
      this.state.listaEpisodios.ordenarPorEstreiaDecrescente();
      flag = true;
    }
    this.setState( state => { return{ ...state, listaEpisodios: this.state.listaEpisodios, estreiaCrescente: flag } } );
  }

  atribuirDatasEstreia(){
    this.state.listaEpisodios.todos.forEach( ep => 
        ep.dataEstreia = this.state.detalhes.find( det => det.episodioId === ep.id ).dataEstreia );  }

  render(){

    return(
      !this.state.listaEpisodios ?
      (<h3>Aguarde...</h3>) :
      <React.Fragment>
        <BotaoUi link='/' classe="verde" nome="Página inicial" />
        <div className="botoes">
          <h3>Modo de ordenação</h3>
          <BotaoUi classe="preto" nome="Data De Estréia" metodo={ this.ordenarPorDataDeEstreia.bind( this ) } />
          <BotaoUi classe='vermelho' nome='Duração' metodo={ this.ordenarPorDuracao.bind( this ) } />
          <p>{ '\n' }</p>
        </div>
        { this.state.listaEpisodios.todos.map( ep => {
          return(
            <React.Fragment>
              <Link to={ { pathname: "/detalhes-episodio", state: ep } } >{ `Episódio: ${ ep.nome } - Nota média: ${ ep.nota } - Temporada: ${ ep.temporada } - Número: ${ ep.ordem }` }</Link>
              <p>{ '\n' }</p>
            </React.Fragment>
          )
          
        } ) }
      </React.Fragment>
    )
  }
}