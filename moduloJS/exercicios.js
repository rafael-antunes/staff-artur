let raio = {
    raio: 5,
    tipoCalculo: "C"
}

function calcularCirculo (raioACalcular) {

    function calcularArea(){
        return Math.PI * Math.pow(raioACalcular.raio, 2);
    }

    function calcularCircunferencia(){
        return 2 * Math.PI * raioACalcular.raio;
    }

    let tipoCalculo = raioACalcular.tipoCalculo;

    if (tipoCalculo === "A") {
        return Math.ceil(calcularArea());
    }
    if (tipoCalculo === "C"){
        return Math.ceil(calcularCircunferencia());
    }   
}

console.log(calcularCirculo(raio));

//FORMA MARCOS///////////////////////////

function calcularCirculoMarcos ({raio, tipoCalculo:tipo}){
    return Math.ceil (tipo == "A" ? Math.PI * Math.pow(raio, 2) : Math.PI * 2 * raio);
}

/////////////////////////////////////////

function naoBissexto (ano) {
    return !((ano % 400 === 0) || (ano % 4 === 0 && ano % 100 !== 0));
}

console.log(naoBissexto(2016));

function somarPares (numeros) {
    let soma = 0;
    for (let i = 0; i < numeros.length; i+=2){
            soma += numeros[i];
    }
    return soma;
}

console.log(somarPares([1, 2, 3, 4, 5, 6, 10]));


function adicionar(primeiroValor){
    return function (segundoValor){
        return primeiroValor + segundoValor;
    }
}

console.log(adicionar(5)(4));

//FORMA MARCOS////////////////////

let adicionarMarcos = valor1 => valor2 => valor1 + valor2; //Arrow function


//Currying

const divisao = divisor => numero => (numero / divisor);
const divisivelPor = divisao(2);

console.log(divisivelPor(4));
console.log(divisivelPor(6));
console.log(divisivelPor(8));
console.log(divisivelPor(10));

///////////////////////////////////


function imprimirBRL (valor) {

    function adicionarPontos (parteInteira){
        parteInteira = parteInteira.split("").reverse();
        let resultado = [];
        let contadorTriades = 1;
        for (let i = 0; i < parteInteira.length; i++){
            resultado.push(parteInteira[i]);
            if(contadorTriades % 3 === 0 && contadorTriades !== 0  && i !== parteInteira.length - 1 && parteInteira[i + 1] !== "-"){
                resultado.push(".");
            }
            contadorTriades++;
        }
        return resultado.reverse().join("");
    }       

    valor = valor.toFixed(2).split(".");
    let valorConvertido = "R$ " + adicionarPontos(valor[0]) + "," + valor[1];
    return valorConvertido;
}

console.log(imprimirBRL(-123456345653.45777));

//Forma feita pelo Marcos no arquivo "moedas.js"


//DESTRUCTION

let objeto = {
    nome: "Marcos",
    idade: 31,
    altura: 1.85
}

const  { nome, altura } = objeto;

console.log(nome);

const arrayTeste = ['Gustavo', 'Kevin', 'Victor', 'Artur'];

let [,pos2,,pos4] = arrayTeste;
console.log(pos4)

//troca de valores entre variáveis
let a = 1;
let b = 3;
[a,b] = [b,a];

//SPREAD OPERATOR

let arraySpread = [1, 77, 83, 42];
console.log(...arraySpread);
let objetoSpread = {...arraySpread}; //transforma array em objeto
console.log(objetoSpread[0]); //elementos seguem sendo acessíveis com colchetes
console.log([..."Meu nome é "]);

