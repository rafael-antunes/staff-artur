package br.com.dbccompany.lotr.Repository;


import java.util.*;

import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonagemRepository<T extends PersonagemEntity> extends CrudRepository<T, Integer> {

    List<T> findAll();
    Optional<T> findById(Integer id);
    List<T> findAllById(Iterable<Integer> id);

    T findByNome (String nome);
    T findByExperiencia(Integer experiencia);
    List<T> findAllByExperiencia(Integer experiencia);
    List<T> findByExperienciaIn(List<Integer> experiencias);
    T findByVida (Double vida);
    List<T> findAllByVida(Double vida);
    List<T> findByVidaIn (List<Double> vidas);
    T findByQntDano (Double qntDano);
    List<T> findAllByQntDano (Double qntDano);
    List<T> findByQntDanoIn (List<Double> qntDanos);
    T findByQntExperienciaPorAtaque (Integer qntExperienciaPorAtaque);
    List<T> findAllByQntExperienciaPorAtaque(Integer qntExperienciaPorAtaque);
    List<T> findByQntExperienciaPorAtaqueIn (List<Integer> qntExperienciaPorAtaque);
    T findByStatus (StatusEnum status);
    List<T> findAllByStatus (StatusEnum status);
    List<T> findByStatusIn (List<StatusEnum> status);
}
