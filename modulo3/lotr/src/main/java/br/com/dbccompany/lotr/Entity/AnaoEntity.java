package br.com.dbccompany.lotr.Entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "anao")
public class AnaoEntity extends PersonagemEntity {

    public AnaoEntity(String nome){
        super(nome);
        super.vida = 110.0;
    }

    public AnaoEntity(){}
}
