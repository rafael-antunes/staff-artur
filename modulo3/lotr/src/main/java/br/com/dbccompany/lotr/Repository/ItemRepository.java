package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.Inventario_X_ItemEntity;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ItemRepository extends CrudRepository<ItemEntity, Integer>{

    Optional<ItemEntity> findById(Integer id);
    Iterable<ItemEntity> findAllById(Iterable<Integer> id);
    List<ItemEntity> findAll();

    List<ItemEntity> findByIdIn (List<Integer> id);
    ItemEntity findByDescricao( String descricao );
    List<ItemEntity> findAllByDescricao(String descricao);
    ItemEntity findByInventarioItemIn (List<Inventario_X_ItemEntity> inventarioItem);
}
