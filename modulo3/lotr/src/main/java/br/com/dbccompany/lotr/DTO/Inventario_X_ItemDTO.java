package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemId;
import br.com.dbccompany.lotr.Entity.ItemEntity;

import java.util.ArrayList;
import java.util.List;

public class Inventario_X_ItemDTO {

    private Integer inventarioId;
    private Integer itemId;
    private Integer quantidade;


    public Inventario_X_ItemDTO (Inventario_X_ItemEntity inventarioItem){
        this.inventarioId = inventarioItem.getId().getId_inventario();
        this.itemId = inventarioItem.getId().getId_item();
        this.quantidade = inventarioItem.getQuantidade();
    }

    public Inventario_X_ItemDTO (){}

    public Inventario_X_ItemEntity converter(){
        Inventario_X_ItemEntity inventarioItem = new Inventario_X_ItemEntity();
        Inventario_X_ItemId id = new Inventario_X_ItemId();
        id.setId_inventario(this.inventarioId);
        id.setId_item(this.itemId);
        inventarioItem.setId(id);
        inventarioItem.setQuantidade(this.quantidade);
        return inventarioItem;
    }

    public Integer getInventarioId() {
        return inventarioId;
    }

    public void setInventarioId(Integer inventarioId) {
        this.inventarioId = inventarioId;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }
}
