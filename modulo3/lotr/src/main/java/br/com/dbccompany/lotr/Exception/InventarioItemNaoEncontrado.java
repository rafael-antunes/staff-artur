package br.com.dbccompany.lotr.Exception;

public class InventarioItemNaoEncontrado extends Inventario_X_ItemException{

    public InventarioItemNaoEncontrado(){
        super("Relacionamento não encontrado!");
    }
}
