package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.AnaoDTO;
import br.com.dbccompany.lotr.Entity.AnaoEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import br.com.dbccompany.lotr.Service.AnaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/api/anao")
public class AnaoController {

    @Autowired
    private AnaoService service;

    @PostMapping(value = "/salvar")
    @ResponseBody
    public AnaoDTO salvarAnao(@RequestBody AnaoDTO anao) {
        return service.salvarAnao(anao.converter());
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public AnaoDTO editarAnao(@RequestBody AnaoDTO anao, @PathVariable Integer id) {
        return service.editarAnao(anao.converter(), id);
    }

    @GetMapping(value = "/")
    @ResponseBody
    public List<AnaoDTO> buscarTodosAnoes() {
        return service.buscarTodosAnoes();
    }

    @GetMapping(value = "/buscarId/{id}")
    @ResponseBody
    public AnaoDTO buscarAnaoPorId(@PathVariable Integer id){
        return service.buscarAnaoPorId(id);
    }

    @GetMapping(value = "/buscarNome/{nome}")
    @ResponseBody
    public AnaoDTO buscarAnaoPorNome(@PathVariable String nome) {
        return service.buscarAnaoPorNome(nome);
    }

    @GetMapping(value = "/buscarXp/{experiencia}")
    @ResponseBody
    public AnaoDTO buscarAnaoPorExperiencia(@PathVariable Integer experiencia) {
        return service.buscarAnaoPorExperiencia(experiencia);
    }

    @GetMapping(value = "/buscarTodosXp/{experiencia}")
    @ResponseBody
    public List<AnaoDTO> buscarTodosAnoesPorExperiencia(@PathVariable Integer experiencia) {
        return service.buscarTodosAnoesPorExperiencia(experiencia);
    }

    @GetMapping(value = "/buscarVida/{vida}")
    @ResponseBody
    public AnaoDTO buscarAnaoPorVida(@PathVariable Double vida) {
        return service.buscarAnaoPorVida(vida);
    }

    @GetMapping(value = "/buscarTodosVida/{vida}")
    @ResponseBody
    public List<AnaoDTO> buscarTodosAnoesPorVida(@PathVariable Double vida) {
        return service.buscarTodosAnoesPorVida(vida);
    }

    @GetMapping(value = "/buscarQntDano/{qntDano}")
    @ResponseBody
    public AnaoDTO buscarAnaoPorQntDano(@PathVariable Double qntDano) {
        return service.buscarAnaoPorQntDano(qntDano);
    }

    @GetMapping(value = "/buscarTodosQntDano/{qntDano}")
    @ResponseBody
    public List<AnaoDTO> buscarTodosAnoesPorQntDano(@PathVariable Double qntDano) {
        return service.buscarTodosAnoesPorQntDano(qntDano);
    }

    @GetMapping(value = "/buscarQntExpAtaque/{qntExpAtaque}")
    @ResponseBody
    public AnaoDTO buscarAnaoPorQntExperienciaPorAtaque(@PathVariable Integer qntExpAtaque) {
        return service.buscarAnaoPorQntExperienciaPorAtaque(qntExpAtaque);
    }

    @GetMapping(value = "/buscarTodosQntExpAtaque/{qntExpAtaque}")
    @ResponseBody
    public List<AnaoDTO> buscarTodosAnoesPorQntExperienciaPorAtaque(@PathVariable Integer qntExpAtaque) {
        return service.buscarTodosAnoesPorQntExperienciaPorAtaque(qntExpAtaque);
    }

    @GetMapping(value = "/buscarStatus/{status}")
    @ResponseBody
    public AnaoDTO buscarAnaoPorStatus(@PathVariable StatusEnum status) {
        return service.buscarAnaoPorStatus(status);
    }

    @GetMapping(value = "/buscarTodosStatus/{status}")
    @ResponseBody
    public List<AnaoDTO> buscarTodosAnoesPorStatus(@PathVariable StatusEnum status) {
        return service.buscarTodosAnoesPorStatus(status);
    }

    @PostMapping (value = "/buscarTodosIds")
    @ResponseBody
    public List<AnaoDTO> buscarTodosPorIds (@RequestBody List<Integer> ids){
        return service.buscarTodosAnoesPorId(ids);
    }

    @PostMapping(value = "/buscarTodosStatusIn/")
    @ResponseBody
    public List<AnaoDTO> buscarAnoesPorStatusIn(@RequestBody List<StatusEnum> status){
        return service.buscarAnoesPorStatusIn(status);
    }

    @PostMapping(value = "/buscarTodosQntExpAtaqueIn")
    @ResponseBody
    public List<AnaoDTO> buscarAnoesPorQntExperienciaPorAtaqueIn(@RequestBody List<Integer> qntsExperienciaPorAtque){
        return service.buscarAnoesPorQntExperienciaPorAtaqueIn(qntsExperienciaPorAtque);
    }

    @PostMapping(value = "/buscarTodosQntDanoIn")
    @ResponseBody
    public List<AnaoDTO> buscarAnoesPorQntDanoIn(@RequestBody List<Double> qntsDano){
        return service.buscarAnoesPorQntDanoIn(qntsDano);
    }

    @PostMapping(value = "/buscarTodosVidaIn")
    @ResponseBody
    public List<AnaoDTO> buscarAnoesPorVidaIn(@RequestBody List<Double> vidas){
        return service.buscarAnoesPorVidaIn(vidas);
    }

    @PostMapping(value = "/buscarTodosXpIn")
    @ResponseBody
    public List<AnaoDTO> buscarAnoesPorExperienciaIn (@RequestBody List<Integer> experiencias){
        return service.buscarAnoesPorExperienciaIn(experiencias);
    }

}