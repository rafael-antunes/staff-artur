package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.DTO.Inventario_X_ItemDTO;
import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemId;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Exception.InventarioErroSalvar;
import br.com.dbccompany.lotr.Exception.InventarioItemErroSalvar;
import br.com.dbccompany.lotr.Exception.InventarioItemNaoEncontrado;
import br.com.dbccompany.lotr.Exception.InventarioNaoEncontrado;
import br.com.dbccompany.lotr.Service.Inventario_X_ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/api/inventarioItem")
public class Inventario_X_ItemController {

    @Autowired
    private Inventario_X_ItemService service;


    @PostMapping(value = "/salvar")
    @ResponseBody
    public Inventario_X_ItemDTO salvarInventarioItem(@RequestBody Inventario_X_ItemDTO inventarioItem) throws InventarioItemErroSalvar {
        try{
            return service.salvar(inventarioItem.converter());
        } catch(Exception e){
            throw new InventarioItemErroSalvar();
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Inventario_X_ItemDTO editarInventarioItem (@RequestBody Inventario_X_ItemDTO inventarioItem, @PathVariable Inventario_X_ItemId id){
        return service.editar(inventarioItem.converter(), id);
    }

    @GetMapping(value = "/")
    @ResponseBody
    public List<Inventario_X_ItemDTO> buscarTodosInventarioItem (){
        return service.buscarTodos();
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public Inventario_X_ItemDTO buscarInventarioItemPorId (@PathVariable Inventario_X_ItemId id) throws InventarioItemNaoEncontrado {
        try{
            return service.buscarPorId(id);
        } catch(Exception e){
            throw new InventarioItemNaoEncontrado();
        }
    }

    @GetMapping(value = "/{quantidade}")
    @ResponseBody
    public Inventario_X_ItemDTO buscarInventarioItemPorQuantidade(@PathVariable Integer quantidade){
        return service.buscarPorQuantidade(quantidade);
    }

    @GetMapping(value = "/buscarTodos/{quantidade}")
    @ResponseBody
    public List<Inventario_X_ItemDTO> buscarTodosInventarioItemPorQuantidade(@PathVariable Integer quantidade){
        return service.buscarTodosPorQuantidade(quantidade);
    }

    @PostMapping(value = "/buscarInventario")
    @ResponseBody
    public Inventario_X_ItemDTO buscarInventarioItemPorInventario(@RequestBody InventarioDTO inventario){
        return service.buscarPorInventario(inventario.converter());
    }

    @PostMapping(value = "/buscarTodosInventario")
    @ResponseBody
    public List<Inventario_X_ItemDTO> buscarTodosInventarioItemPorInventario(@RequestBody InventarioDTO inventario){
        return service.buscarTodosPorInventario(inventario.converter());
    }

    @PostMapping(value = "/buscarItem")
    @ResponseBody
    public Inventario_X_ItemDTO buscarInventarioItemPorItem(@RequestBody ItemDTO item){
        return service.buscarPorItem(item.converter());
    }

    @PostMapping(value = "/buscarTodosItem")
    @ResponseBody
    public List<Inventario_X_ItemDTO> buscarTodosInventarioItemPorItem(@RequestBody ItemDTO item){
        return service.buscarTodosPorItem(item.converter());
    }

    @PostMapping(value = "/buscarTodosQuantidadeIn")
    @ResponseBody
    public List<Inventario_X_ItemDTO> buscarTodosInventarioItemPorInventarioIn(@RequestBody List<Integer> quantidades){
        return service.buscarTodosPorQuantidades(quantidades);
    }

}
