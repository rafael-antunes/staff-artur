package br.com.dbccompany.lotr.Exception;

public class InventarioItemErroSalvar extends Inventario_X_ItemException{

    public InventarioItemErroSalvar(){
        super("Impossível salvar relacionamento!");
    }
}
