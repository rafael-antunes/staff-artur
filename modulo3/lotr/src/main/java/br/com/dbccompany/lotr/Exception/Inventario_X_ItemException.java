package br.com.dbccompany.lotr.Exception;

public class Inventario_X_ItemException extends Exception{

    private String mensagem;

    public Inventario_X_ItemException(String mensagem){
        super(mensagem);
    }

    public String getMensagem() {
        return mensagem;
    }
}
