package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.AnaoDTO;
import br.com.dbccompany.lotr.DTO.ElfoDTO;
import br.com.dbccompany.lotr.Entity.ElfoEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import br.com.dbccompany.lotr.Service.ElfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/api/elfo")
public class ElfoController {

    @Autowired
    private ElfoService service;

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ElfoDTO salvarElfo (@RequestBody ElfoDTO elfo){
        return service.salvarElfo(elfo.converter());
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ElfoDTO editarElfo (@RequestBody ElfoDTO elfo, @PathVariable Integer id){
        return service.editarElfo(elfo.converter(), id);
    }

    @GetMapping(value = "/")
    @ResponseBody
    public List<ElfoDTO> buscarTodosElfos(){
        return service.buscarTodosElfos();
    }

    @GetMapping(value = "/buscarId/{id}")
    @ResponseBody
    public ElfoDTO buscarElfoPorId(@PathVariable Integer id){
        return service.buscarElfoPorId(id);
    }

    @GetMapping(value = "/buscarNome/{nome}")
    @ResponseBody
    public ElfoDTO buscarElfoPorNome (@PathVariable String nome){
        return service.buscarElfoPorNome(nome);
    }

    @GetMapping(value = "/buscarXp/{experiencia}")
    @ResponseBody
    public ElfoDTO buscarElfoPorExperiencia (@PathVariable Integer experiencia){
        return service.buscarElfoPorExperiencia(experiencia);
    }

    @GetMapping(value = "/buscarTodosXp/{experiencia}")
    @ResponseBody
    public List<ElfoDTO> buscarTodosElfosPorExperiencia (@PathVariable Integer experiencia){
        return service.buscarTodosElfosPorExperiencia(experiencia);
    }

    @GetMapping(value = "/buscarVida/{vida}")
    @ResponseBody
    public ElfoDTO buscarElfoPorVida (@PathVariable Double vida){
        return service.buscarElfoPorVida(vida);
    }

    @GetMapping(value = "/buscarTodosVida/{vida}")
    @ResponseBody
    public List<ElfoDTO> buscarTodosElfosPorVida (@PathVariable Double vida){
        return service.buscarTodosElfosPorVida(vida);
    }

    @GetMapping(value = "/buscarQntDano/{qntDano}")
    @ResponseBody
    public ElfoDTO buscarElfoPorQntDano (@PathVariable Double qntDano){
        return service.buscarElfoPorQntDano(qntDano);
    }

    @GetMapping(value = "/buscarTodosQntDano/{qntDano}")
    @ResponseBody
    public List<ElfoDTO> buscarTodosElfosPorQntDano (@PathVariable Double qntDano){
        return service.buscarTodosElfosPorQntDano(qntDano);
    }

    @GetMapping(value = "/buscarQntExpAtaque/{qntExpAtaque}")
    @ResponseBody
    public ElfoDTO buscarElfoPorQntExperienciaPorAtaque (@PathVariable Integer qntExpAtaque){
        return service.buscarElfoPorQntExperienciaPorAtaque(qntExpAtaque);
    }

    @GetMapping(value = "/buscarTodosQntExpAtaque/{qntExpAtaque}")
    @ResponseBody
    public List<ElfoDTO> buscarTodosElfosPorQntExperienciaPorAtaque (@PathVariable Integer qntExpAtaque){
        return service.buscarTodosElfosPorQntExperienciaPorAtaque(qntExpAtaque);
    }

    @GetMapping(value = "/buscarStatus/{status}")
    @ResponseBody
    public ElfoDTO buscarElfoPorStatus (@PathVariable StatusEnum status){
        return service.buscarElfoPorStatus(status);
    }

    @GetMapping(value = "/buscarTodosStatus/{status}")
    @ResponseBody
    public List<ElfoDTO> buscarTodosElfosPorStatus (@PathVariable StatusEnum status){
        return service.buscarTodosElfosPorStatus(status);
    }

    @PostMapping (value = "/buscarTodosIds")
    @ResponseBody
    public List<ElfoDTO> buscarTodosPorIds (@RequestBody List<Integer> ids){
        return service.buscarTodosElfosPorId(ids);
    }

    @PostMapping(value = "/buscarTodosStatusIn/")
    @ResponseBody
    public List<ElfoDTO> buscarElfosPorStatusIn(@RequestBody List<StatusEnum> status){
        return service.buscarElfosPorStatusIn(status);
    }

    @PostMapping(value = "/buscarTodosQntExpAtaqueIn")
    @ResponseBody
    public List<ElfoDTO> buscarElfosPorQntExperienciaPorAtaqueIn(@RequestBody List<Integer> qntsExperienciaPorAtaque){
        return service.buscarElfosPorQntExperienciaPorAtaqueIn(qntsExperienciaPorAtaque);
    }

    @PostMapping(value = "/buscarTodosQntDanoIn")
    @ResponseBody
    public List<ElfoDTO> buscarElfosPorQntDanoIn(@RequestBody List<Double> qntsDano){
        return service.buscarElfosPorQntDanoIn(qntsDano);
    }

    @PostMapping(value = "/buscarTodosVidaIn")
    @ResponseBody
    public List<ElfoDTO> buscarElfosPorVidaIn(@RequestBody List<Double> vidas){
        return service.buscarElfosPorVidaIn(vidas);
    }

    @PostMapping(value = "/buscarTodosXpIn")
    @ResponseBody
    public List<ElfoDTO> buscarElfosPorExperienciaIn (@RequestBody List<Integer> experiencias){
        return service.buscarElfosPorExperienciaIn(experiencias);
    }
}
