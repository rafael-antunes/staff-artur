package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemEntity;

import br.com.dbccompany.lotr.Entity.Inventario_X_ItemId;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface Inventario_X_ItemRepository extends CrudRepository<Inventario_X_ItemEntity, Inventario_X_ItemId> {

    List<Inventario_X_ItemEntity> findAll();
    Optional<Inventario_X_ItemEntity> findById( Inventario_X_ItemId id);

    Inventario_X_ItemEntity findByQuantidade (Integer quantidade);
    List<Inventario_X_ItemEntity> findAllByQuantidade (Integer quantidade);
    List<Inventario_X_ItemEntity> findByQuantidadeIn (List<Integer> quantidades);

    Inventario_X_ItemEntity findByInventario (InventarioEntity inventario);
    List<Inventario_X_ItemEntity> findAllByInventario (InventarioEntity inventario);
    List<Inventario_X_ItemEntity> findByInventarioIn (List<InventarioEntity> inventarios);

    Inventario_X_ItemEntity findByItem (ItemEntity item);
    List<Inventario_X_ItemEntity> findAllByItem (ItemEntity item);
    List<Inventario_X_ItemEntity> findByItemIn (List<ItemEntity> itens);
}
