package br.com.dbccompany.lotr.Entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "elfo")
public class ElfoEntity extends PersonagemEntity{

    public ElfoEntity(String nome){
        super(nome);
        super.vida = 110.0;
    }

    public ElfoEntity(){}
}
