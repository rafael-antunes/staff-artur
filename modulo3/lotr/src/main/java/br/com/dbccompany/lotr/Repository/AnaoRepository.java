package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.AnaoEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface AnaoRepository extends PersonagemRepository<AnaoEntity>{
}
