package br.com.dbccompany.lotr.Exception;

public class InventarioErroSalvar extends InventarioException{

    public InventarioErroSalvar(){
        super("Impossível salvar inventário!");
    }
}
