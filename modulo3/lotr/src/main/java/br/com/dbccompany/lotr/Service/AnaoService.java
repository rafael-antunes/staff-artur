package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.AnaoDTO;
import br.com.dbccompany.lotr.DTO.ElfoDTO;
import br.com.dbccompany.lotr.Entity.AnaoEntity;
import br.com.dbccompany.lotr.Entity.ElfoEntity;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import br.com.dbccompany.lotr.Repository.AnaoRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AnaoService extends PersonagemService<AnaoRepository, AnaoEntity> {
    public AnaoDTO salvarAnao (AnaoEntity anao){
        return new AnaoDTO(super.salvar(anao));
    }
    public AnaoDTO editarAnao (AnaoEntity anao, Integer id){
        return new AnaoDTO(super.editar(anao, id));
    }
    public AnaoDTO buscarAnaoPorId (Integer id){
        return new AnaoDTO(super.buscarPorId(id));
    }

    private List<AnaoDTO> listaEntityParaDBO(List<AnaoEntity> listaEntity){
        List<AnaoDTO> anoesDTO = new ArrayList<>();
        for(AnaoEntity anao : listaEntity){
            anoesDTO.add(new AnaoDTO(anao));
        }
        return anoesDTO;
    }
    public List<AnaoDTO> buscarTodosAnoesPorId(List<Integer> ids){
        return listaEntityParaDBO(super.buscarTodosPorId(ids));
    }

    public List<AnaoDTO> buscarTodosAnoes(){
        return listaEntityParaDBO(super.buscarTodosPersonagens());
    }
    public AnaoDTO buscarAnaoPorNome (String nome){
        return new AnaoDTO(super.buscarPorNome(nome));
    }
    public AnaoDTO buscarAnaoPorExperiencia(Integer experiencia){
        return new AnaoDTO(super.buscarPorExperiencia(experiencia));
    }
    public List<AnaoDTO> buscarTodosAnoesPorExperiencia(Integer experiencia){
        return listaEntityParaDBO(super.buscarTodosPorExperiencia(experiencia));
    }
    public List<AnaoDTO> buscarAnoesPorExperienciaIn(List<Integer> experiencias) {
        return listaEntityParaDBO(super.buscarTodosPorExperienciaIn(experiencias));
    }
    public AnaoDTO buscarAnaoPorVida(Double vida){
        return new AnaoDTO(super.buscarPorVida(vida));
    }
    public List<AnaoDTO> buscarTodosAnoesPorVida(Double vida) {
        return listaEntityParaDBO(super.buscarTodosPorVida(vida));
    }
    public List<AnaoDTO> buscarAnoesPorVidaIn(List<Double> vidas) {
        return listaEntityParaDBO(super.buscarTodosPorVidaIn(vidas));
    }
    public AnaoDTO buscarAnaoPorQntDano(Double qntDano){
        return new AnaoDTO(super.buscarPorQntDano(qntDano));
    }
    public List<AnaoDTO> buscarTodosAnoesPorQntDano(Double qntDano){
        return listaEntityParaDBO(super.buscarTodosPorQntDano(qntDano));
    }
    public List<AnaoDTO> buscarAnoesPorQntDanoIn (List<Double> qntsDano) {
        return listaEntityParaDBO(super.buscarTodosPorQntDanoIn(qntsDano));
    }
    public AnaoDTO buscarAnaoPorQntExperienciaPorAtaque (Integer qntExperienciaPorAtaque){
        return new AnaoDTO(super.buscarPorQntExperienciaPorAtaque(qntExperienciaPorAtaque));
    }
    public List<AnaoDTO> buscarTodosAnoesPorQntExperienciaPorAtaque(Integer qntExperienciaPorAtaque){
        return listaEntityParaDBO(super.buscarTodosPorQntExperienciaPorAtaque(qntExperienciaPorAtaque));
    }
    public List<AnaoDTO> buscarAnoesPorQntExperienciaPorAtaqueIn(List<Integer> qntsExperienciaPorAtaque){
        return listaEntityParaDBO(super.buscarTodosQntExperienciaPorAtaqueIn(qntsExperienciaPorAtaque));
    }
    public AnaoDTO buscarAnaoPorStatus(StatusEnum status){
        return new AnaoDTO(super.buscarPorStatus(status));
    }
    public List<AnaoDTO> buscarTodosAnoesPorStatus(StatusEnum status){
        return listaEntityParaDBO(super.buscarTodosPorStatus(status));
    }
    public List<AnaoDTO> buscarAnoesPorStatusIn(List<StatusEnum> status){
        return listaEntityParaDBO(super.buscarTodosPorStatusIn(status));
    }
}
