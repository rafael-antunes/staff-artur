package br.com.dbccompany.lotr.Service;


import br.com.dbccompany.lotr.Entity.PersonagemEntity;

import br.com.dbccompany.lotr.Entity.StatusEnum;
import br.com.dbccompany.lotr.Repository.PersonagemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public abstract class PersonagemService<R extends PersonagemRepository<E>, E extends PersonagemEntity>  {


    @Autowired
    private R repository;

    @Transactional( rollbackFor = Exception.class )
    public E salvar(E e ){
        return this.salvarEEditar(e);
    }
    @Transactional(rollbackFor = Exception.class)
    public E editar( E e, Integer id){
        e.setId(id);
        return this.salvarEEditar(e);
    }
    @Transactional(rollbackFor = Exception.class)
    protected E salvarEEditar(E e){
        return repository.save (e);
    }

    public E buscarPorId(Integer id){
        Optional<E> personagem = repository.findById(id);
        if(personagem.isPresent()) {
            return repository.findById(id).get();
        }
        return null;
    }
    public List<E> buscarTodosPorId(List<Integer> ids){
        return repository.findAllById(ids);
    }
    public List<E> buscarTodosPersonagens(){
        return repository.findAll();
    }
    public E buscarPorNome (String nome){
        return repository.findByNome(nome);
    }
    public E buscarPorExperiencia(Integer experiencia){
        return repository.findByExperiencia(experiencia);
    }
    public List<E> buscarTodosPorExperiencia(Integer experiencia){
        return repository.findAllByExperiencia(experiencia);
    }
    public List<E> buscarTodosPorExperienciaIn(List<Integer> experiencias){
        return repository.findByExperienciaIn(experiencias);
    }
    public E buscarPorVida(Double vida){
        return repository.findByVida(vida);
    }
    public List<E> buscarTodosPorVida(Double vida){
        return repository.findAllByVida(vida);
    }
    public List<E> buscarTodosPorVidaIn (List<Double> vidas){
        return repository.findByVidaIn(vidas);
    }
    public E buscarPorQntDano(Double qntDano){
        return repository.findByQntDano(qntDano);
    }
    public List<E> buscarTodosPorQntDano(Double qntDano){
        return repository.findAllByQntDano(qntDano);
    }
    public List<E> buscarTodosPorQntDanoIn(List<Double> qntDanos){
        return repository.findByQntDanoIn(qntDanos);
    }
    public E buscarPorQntExperienciaPorAtaque(Integer qntExperienciaPorAtaque){
        return repository.findByQntExperienciaPorAtaque(qntExperienciaPorAtaque);
    }
    public List<E> buscarTodosPorQntExperienciaPorAtaque(Integer qntExperienciaPorAtaque){
        return repository.findAllByQntExperienciaPorAtaque(qntExperienciaPorAtaque);
    }
    public List<E> buscarTodosQntExperienciaPorAtaqueIn(List<Integer> qntExperienciaPorAtaques){
        return repository.findByQntExperienciaPorAtaqueIn(qntExperienciaPorAtaques);
    }
    public E buscarPorStatus(StatusEnum status){
        return repository.findByStatus(status);
    }
    public List<E> buscarTodosPorStatus(StatusEnum status){
        return repository.findAllByStatus(status);
    }
    public List<E> buscarTodosPorStatusIn(List<StatusEnum> status){
        return repository.findByStatusIn(status);
    }
}
