package br.com.dbccompany.lotr.Security;

import br.com.dbccompany.lotr.Entity.UsuarioEntity;
import br.com.dbccompany.lotr.Repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService implements UserDetailsService {

    @Autowired
    private UsuarioRepository repository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UsuarioEntity usuario = repository.findByUsername(username);
        if(usuario != null){
            return usuario;
        }
        throw new UsernameNotFoundException("Usuário inválido.");
    }
}
