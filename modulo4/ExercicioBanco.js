alfa = {
    codigo: 011,
    nome: "Alfa",
    agencia: [
        {
            codigo: 0001,
            nome: "Web",
            endereco: {
                Logradouro: "Rua Testando",
                numero: 55,
                complemento: "Loja 1",
                bairro: "NA",
                cidade: "NA",
                estado: "NA",
                pais: "Brasil"    
            },
            consolidacao: [
                {
                    saldoAtual: 0,
                    saques: 0,
                    depositos: 0,
                    numeroCorrentistas: 0
                }
            ],
            conta: [
                {
                    codigo: 1,
                    tipo: "PJ",
                    saldo: 100000,
                    gerente: [
                        {
                            nome: "Paulo",
                            cpf: "234.543.200-89",
                            estadoCivil: "Solteiro",
                            dataNascimento: "23/12/1876",
                            codigoFuncionafio: "0001",
                            tipoGerente: "GC",
                            endereco: {
                                logradouro: "Rua Testada",
                                numero: 1900,
                                complemento: "Ap 102",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"
                            }
                        }
                    ],
                    cliente: [
                        {
                            cpf: "365.800.000-00",
                            nome: "João",
                            dataNascimento: "25/05/1979",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Testada",
                                numero: 1900,
                                complemento: "Ap 102",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                        
                            }
                        },
                        {
                            cpf: "365.800.000-01",
                            nome: "Jorge",
                            dataNascimento: "25/05/1979",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Testada",
                                numero: 1900,
                                complemento: "Ap 102",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }
                        },
                        {
                            cpf: "365.800.000-02",
                            nome: "Joaquim",
                            dataNascimento: "25/05/1979",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Testada",
                                numero: 1900,
                                complemento: "Ap 102",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }
                        }
                    ],
                    movimentacoes: [
                        {
                            tipo: "debito",
                            valor: 5540.00
                        },
                        {
                            tipo: "credito",
                            valor: 1234.00
                        },
                        {
                            tipo: "debito",
                            valor: 5543.00
                        }
                    ]
                },
                {
                    codigo: 2,
                    tipo: "PJ",
                    saldo: 10000000,
                    cliente: [
                        {
                            cpf: "365.800.000-03",
                            nome: "Gabriel",
                            dataNascimento: "25/05/1979",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Testada",
                                numero: 1900,
                                complemento: "Ap 102",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }
                        },
                        {
                            cpf: "365.800.000-04",
                            nome: "Tanise",
                            dataNascimento: "25/05/1979",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Testada",
                                numero: 1900,
                                complemento: "Ap 102",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }                                
                            
                        },
                        {
                            cpf: "365.800.000-05",
                            nome: "Jessica",
                            dataNascimento: "25/05/1979",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Testada",
                                numero: 1900,
                                complemento: "Ap 102",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }                               
                            
                        }
                    ]
                },
                {
                    codigo: 3,
                    tipo: "CONJ",
                    saldo: 150000,
                    cliente: [
                        {
                            cpf: "365.800.000-06",
                            nome: "Paulo",
                            dataNascimento: "12/12/2001",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Largada",
                                numero: 435,
                                complemento: "Ap 123",
                                bairro: "Petropolis",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"
                            }
                        },
                        {
                            cpf: "365.800.000-07",
                            nome: "Rodrigo",
                            dataNascimento: "12/12/1981",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Gastada",
                                numero: 432,
                                complemento: "Ap 463",
                                bairro: "Petropolis",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"
                            }
                        }
                    ]
                }
            ]
        },
        {
            codigo: 0002,
            nome: "California",
            endereco: {
                Logradouro: "Rua Testing",
                numero: 122,
                bairro: "Between Hyde And Powell Streets",
                cidade: "San Francisco",
                Estado: "California",
                Pais: "EUA"
            },
            consolidacao: {
                saldoAtual: 0,
                saques: 0,
                depositos: 0,
                numeroCorrentistas: 0
            },
            conta: [
                {
                    codigo: 4,
                    tipo: "CONJ",
                    saldo: 100000,
                    cliente: [
                        {
                            cpf: "365.800.000-08",
                            nome: "James",
                            dataNascimento: "12/12/1981",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Gastada",
                                numero: 432,
                                complemento: "Ap 463",
                                bairro: "Petropolis",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"
                            }
                        },
                        {
                            cpf: "365.800.000-09",
                            nome: "Carla",
                            dataNascimento: "12/12/1981",
                            estadoCivil: "Casado",
                            endereco: {
                                logradouro: "Rua Falhada",
                                numero: 432,
                                complemento: "Ap 765",
                                bairro: "Partenon",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"
                            }
                        }
                    ]
                },
                {
                    codigo: 5,
                    tipo: "CONJ",
                    saldo: 10000000,
                    cliente: [
                        {
                            cpf: "365.800.000-08",
                            nome: "James",
                            dataNascimento: "12/12/1981",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Gastada",
                                numero: 432,
                                complemento: "Ap 463",
                                bairro: "Petropolis",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"
                            }
                        },
                        {
                            cpf: "365.800.000-09",
                            nome: "Carla",
                            dataNascimento: "12/12/1981",
                            estadoCivil: "Casado",
                            endereco: {
                                logradouro: "Rua Falhada",
                                numero: 432,
                                complemento: "Ap 765",
                                bairro: "Partenon",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"
                            }
                        }
                    ]
                },
                {
                    codigo: 6,
                    tipo: "PF",
                    saldo: 150000,
                    cliente: [
                        {
                            cpf: "365.800.000-10",
                            nome: "Fabricio",
                            dataNascimento: "12/12/1976",
                            estadoCivil: "Casado",
                            endereco: {
                                logradouro: "Rua Coalhada",
                                numero: 432,
                                complemento: "Casa",
                                bairro: "Partenon",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"
                            }
                        }
                    ]
                }
            ]
        },
        {
            codigo: 0101,
            nome: "Londres",
            endereco: {
                Logradouro: "Rua Tesing",
                numero: 525,
                bairro: "Croydon",
                cidade: "Londres",
                estado: "Boroughs",
                pais: "England"
            },
            consolidacao: {
                saldoAtual: 0,
                saques: 0,
                depositos: 0,
                numeroCorrentistas: 0
            },
            conta: [
                {
                    codigo: 7,
                    tipo: "PF",
                    saldo: 100000,
                    cliente: [
                        {
                            cpf: "365.800.000-11",
                            nome: "Rodrigo",
                            dataNascimento: "12/12/1976",
                            estadoCivil: "Casado",
                            endereco: {
                                logradouro: "Rua Coalhada",
                                numero: 432,
                                complemento: "Casa",
                                bairro: "Partenon",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"
                            }
                        }
                    ]
                },
                {
                    codigo: 8,
                    tipo: "PF",
                    saldo: 10000000,
                    cliente: [
                        {
                            cpf: "365.800.000-12",
                            nome: "Henrique",
                            dataNascimento: "12/12/1976",
                            estadoCivil: "Casado",
                            endereco: {
                                logradouro: "Rua Coalhada",
                                numero: 432,
                                complemento: "Casa",
                                bairro: "Partenon",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"
                            }
                        }
                    ]
                },
                {
                    codigo: 9,
                    tipo: "INVEST",
                    saldo: 150000,
                    cliente: [
                        {
                            cpf: "365.800.000-13",
                            nome: "Daniel",
                            dataNascimento: "12/12/1976",
                            estadoCivil: "Casado",
                            endereco: {
                                logradouro: "Rua Coalhada",
                                numero: 432,
                                complemento: "Casa",
                                bairro: "Partenon",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"
                            }
                        }
                    ]
                },
                {
                    codigo: 10,
                    tipo: "INVEST",
                    saldo: 150000,
                    cliente: [
                        {
                            cpf: "365.800.000-13",
                            nome: "Daniel",
                            dataNascimento: "12/12/1976",
                            estadoCivil: "Casado",
                            endereco: {
                                logradouro: "Rua Coalhada",
                                numero: 432,
                                complemento: "Casa",
                                bairro: "Partenon",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"
                            }
                        }
                    ]
                }                                
            ]
        }
    ]
}


beta = {
    codigo: 241,
    nome: "Beta",
    agencia: [
        {
            codigo: 0001,
            nome: "Web",
            endereco: {
                logradouro: "Rua Testando",
                numero: 55,
                complemento: "Loja 2",
                bairro: "NA",
                cidade: "NA",
                estado: "NA",
                pais: "Brasil"
            },
            consolidacao: {
                saldoAtual: 0,
                saques: 0,
                depositos: 0,
                numeroCorrentistas: 0
            },
            conta: [
                {
                    codigo: 11,
                    tipo: "PJ",
                    saldo: 100000,
                    cliente: [
                        {
                            cpf: "365.800.000-13",
                            nome: "Gabriel",
                            dataNascimento: "25/05/1979",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Testada",
                                numero: 1900,
                                complemento: "Ap 102",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }
                        },
                        {
                            cpf: "365.800.000-14",
                            nome: "Tanise",
                            dataNascimento: "25/05/1979",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Testada",
                                numero: 1900,
                                complemento: "Ap 102",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }                                
                            
                        },
                        {
                            cpf: "365.800.000-15",
                            nome: "Jessica",
                            dataNascimento: "25/05/1979",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Testada",
                                numero: 1900,
                                complemento: "Ap 102",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }                               
                        }
                    ]
                },
                {
                    codigo: 12,
                    tipo: "PJ",
                    saldo: 10000000,
                    cliente: [
                        {
                            cpf: "365.800.000-23",
                            nome: "Gabriel",
                            dataNascimento: "25/05/1979",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Testada",
                                numero: 1900,
                                complemento: "Ap 102",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }
                        },
                        {
                            cpf: "365.800.000-24",
                            nome: "Tanise",
                            dataNascimento: "25/05/1987",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Testada",
                                numero: 1900,
                                complemento: "Ap 102",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }                                
                            
                        },
                        {
                            cpf: "365.800.000-25",
                            nome: "Jessica",
                            dataNascimento: "25/05/1967",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Testada",
                                numero: 1900,
                                complemento: "Ap 102",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }                               
                        }
                    ]
                },
                {
                    codigo: 13,
                    tipo: "CONJ",
                    saldo: 150000,
                    cliente: [
                        {
                            cpf: "365.800.000-33",
                            nome: "Gabriel",
                            dataNascimento: "25/05/1979",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Testada",
                                numero: 1900,
                                complemento: "Ap 102",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }
                        },
                        {
                            cpf: "365.800.000-34",
                            nome: "Tanise",
                            dataNascimento: "25/05/1987",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Testada",
                                numero: 1900,
                                complemento: "Ap 102",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }                                  
                        }
                    ]
                },
                {
                    codigo: 14,
                    tipo: "CONJ",
                    saldo: 150000,
                    cliente: [
                        {
                            cpf: "365.800.000-43",
                            nome: "Gabriel",
                            dataNascimento: "25/05/1979",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Testada",
                                numero: 1900,
                                complemento: "Ap 102",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }
                        },
                        {
                            cpf: "365.800.000-44",
                            nome: "Fabia",
                            dataNascimento: "25/05/1987",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Testada",
                                numero: 1900,
                                complemento: "Ap 102",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }                                  
                        }
                    ]
                },
                {
                    codigo: 15,
                    tipo: "CONJ",
                    saldo: 100000,
                    cliente: [
                        {
                            cpf: "365.800.000-53",
                            nome: "Eduardo",
                            dataNascimento: "25/05/1979",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Testada",
                                numero: 1900,
                                complemento: "Ap 102",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }
                        },
                        {
                            cpf: "365.800.000-54",
                            nome: "Rafaela",
                            dataNascimento: "25/05/1987",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Testada",
                                numero: 1900,
                                complemento: "Ap 102",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }                                  
                        }
                    ]
                },
                {
                    codigo: 16,
                    tipo: "PF",
                    saldo: 10000000,
                    cliente: [
                        {
                            cpf: "365.800.000-64",
                            nome: "Henrique",
                            dataNascimento: "25/05/1977",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Fagulha",
                                numero: 561,
                                complemento: "Ap 123",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }
                        }
                    ]
                },
                {
                    codigo: 17,
                    tipo: "PF",
                    saldo: 150000,
                    cliente: [
                        {
                            cpf: "365.800.000-65",
                            nome: "Valeria",
                            dataNascimento: "25/05/1977",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Fagulha",
                                numero: 561,
                                complemento: "Ap 123",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }
                        }
                    ]
                },
                {
                    codigo: 18,
                    tipo: "PF",
                    saldo: 150000,
                    cliente: [
                        {
                            cpf: "365.800.000-66",
                            nome: "Willian",
                            dataNascimento: "25/05/1977",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Fagulha",
                                numero: 561,
                                complemento: "Ap 123",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }
                        }
                    ]
                },
                {
                    codigo: 19,
                    tipo: "INVEST",
                    saldo: 150000,
                    cliente: [
                        {
                            cpf: "365.800.000-67",
                            nome: "Daniel",
                            dataNascimento: "25/05/1977",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Fagulha",
                                numero: 561,
                                complemento: "Ap 123",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }
                        }
                    ]
                },
                {
                    codigo: 20,
                    tipo: "INVEST",
                    saldo: 150000,
                    cliente: [
                        {
                            cpf: "365.800.000-68",
                            nome: "Pedro",
                            dataNascimento: "25/05/1977",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Fagulha",
                                numero: 561,
                                complemento: "Ap 123",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }
                        }
                    ]
                }
            ]
        }
    ]
};

omega = {
    codigo: 307,
    nome: "Omega",
    agencia: [
        {
            codigo: 0001,
            nome: "Web",
            endereco: {
                logradouro: "Rua Testando",
                numero: 55,
                complemento: "Loja 3",
                bairro: "NA",
                cidade: "NA",
                estado: "NA",
                pais: "Brasil"
            },
            consolidacao: {
                saldoAtual: 0,
                saques: 0,
                depositos: 0,
                numeroCorrentistas: 0
            },
            conta: [
                {
                    codigo: 21,
                    tipo: "PJ",
                    saldo: 100000,
                    cliente: [
                        {
                            cpf: "365.800.000-64",
                            nome: "Valeria",
                            dataNascimento: "25/05/1977",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Fagulha",
                                numero: 561,
                                complemento: "Ap 123",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }
                        },
                        {
                            cpf: "365.800.000-80",
                            nome: "Enver",
                            dataNascimento: "25/05/1977",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Fagulha",
                                numero: 561,
                                complemento: "Ap 123",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }
                        },
                        {
                            cpf: "123.800.000-80",
                            nome: "Ursula",
                            dataNascimento: "25/05/1977",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Fagulha",
                                numero: 561,
                                complemento: "Ap 123",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }
                        }
                    ]
                },
                {
                    codigo: 22,
                    tipo: "PJ",
                    saldo: 10000000,
                    cliente: [
                        {
                            cpf: "225.800.000-64",
                            nome: "Mariana",
                            dataNascimento: "25/05/1977",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Fagulha",
                                numero: 561,
                                complemento: "Ap 123",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }
                        },
                        {
                            cpf: "445.800.000-80",
                            nome: "Valeria",
                            dataNascimento: "25/05/1977",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Fagulha",
                                numero: 561,
                                complemento: "Ap 123",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }
                        },
                        {
                            cpf: "765.800.000-80",
                            nome: "Yolanda",
                            dataNascimento: "25/05/1977",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Fagulha",
                                numero: 561,
                                complemento: "Ap 123",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }
                        }
                    ]
                },
                {
                    codigo: 23,
                    tipo: "CONJ",
                    saldo: 150000,
                    cliente: [
                        {
                            cpf: "365.800.000-64",
                            nome: "Tanise",
                            dataNascimento: "25/05/1977",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Fagulha",
                                numero: 561,
                                complemento: "Ap 123",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }
                        },
                        {
                            cpf: "365.800.000-80",
                            nome: "Valeria",
                            dataNascimento: "25/05/1977",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Fagulha",
                                numero: 561,
                                complemento: "Ap 123",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }
                        }
                    ]
                }
            ]
        },
        {
            codigo: 8761,
            nome: "Itu",
            endereco: {
                logradouro: "Rua do Meio",
                numero: 2233,
                bairro: "Qualquer",
                cidade: "Itu",
                estado: "São Paulo",
                pais: "Brasil"
            },
            consolidacao: {
                saldoAtual: 0,
                saques: 0,
                depositos: 0,
                numeroCorrentistas: 0
            },
            conta: [
                {
                    codigo: 24,
                    tipo: "CONJ",
                    saldo: 100000,
                    cliente: [
                        {
                            cpf: "365.800.000-64",
                            nome: "Valeria",
                            dataNascimento: "25/05/1977",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Fagulha",
                                numero: 561,
                                complemento: "Ap 123",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }
                        },
                        {
                            cpf: "365.800.554-64",
                            nome: "Tiago",
                            dataNascimento: "25/05/1977",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Fagulha",
                                numero: 561,
                                complemento: "Ap 123",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }
                        }
                    ]
                },
                {
                    codigo: 25,
                    tipo: "CONJ",
                    saldo: 10000000,
                    cliente: [
                        {
                            cpf: "365.800.000-64",
                            nome: "Valeria",
                            dataNascimento: "25/05/1977",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Fagulha",
                                numero: 561,
                                complemento: "Ap 123",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }
                        },
                        {
                            cpf: "365.800.776-64",
                            nome: "Ketlin",
                            dataNascimento: "25/05/1977",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Fagulha",
                                numero: 561,
                                complemento: "Ap 123",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }
                        }
                    ]
                },
                {
                    codigo: 26,
                    tipo: "PF",
                    saldo: 150000,
                    cliente: [
                        {
                            cpf: "365.987.000-64",
                            nome: "Fabiana",
                            dataNascimento: "25/05/1977",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Fagulha",
                                numero: 561,
                                complemento: "Ap 123",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }
                        }
                    ]
                }
            ]
        },
        {
            codigo: 4567,
            nome: "Hermana",
            endereco: {
                logradouro: "Rua do Boca",
                numero: 222,
                bairro: "Caminito",
                cidade: "Buenos Aires",
                estado: "Buenos Aires",
                pais: "Argentina"
            },
            consolidacao: {
                saldoAtual: 0,
                saques: 0,
                depositos: 0,
                numeroCorrentistas: 0
            },
            conta: [
                {
                    codigo: 27,
                    tipo: "PF",
                    saldo: 100000,
                    cliente: [
                        {
                            cpf: "365.776.000-64",
                            nome: "Vanderlise",
                            dataNascimento: "25/05/1977",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Fagulha",
                                numero: 561,
                                complemento: "Ap 123",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }
                        }
                    ]
                },
                {
                    codigo: 28,
                    tipo: "PF",
                    saldo: 10000000,
                    cliente: [
                        {
                            cpf: "365.232.000-64",
                            nome: "Joao",
                            dataNascimento: "25/05/1977",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Fagulha",
                                numero: 561,
                                complemento: "Ap 123",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }
                        }
                    ]
                },
                {
                    codigo: 29,
                    tipo: "INVEST",
                    saldo: 150000,
                    cliente: [
                        {
                            cpf: "365.765.000-64",
                            nome: "Ulinda",
                            dataNascimento: "25/05/1977",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Fagulha",
                                numero: 561,
                                complemento: "Ap 123",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }
                        }
                    ]
                },
                {
                    codigo: 30,
                    tipo: "INVEST",
                    saldo: 150000,
                    cliente: [
                        {
                            cpf: "365.345.000-64",
                            nome: "Margarida",
                            dataNascimento: "25/05/1977",
                            estadoCivil: "Solteiro",
                            endereco: {
                                logradouro: "Rua Fagulha",
                                numero: 561,
                                complemento: "Ap 123",
                                bairro: "Centro",
                                cidade: "Porto Alegre",
                                estado: "RS",
                                pais: "Brasil"                                
                            }
                        }
                    ]
                }
            ]
        }
    ]
}

db.createCollection("banco");
db.banco.insert(alfa);
db.banco.insert(beta);
db.banco.insert(omega);
