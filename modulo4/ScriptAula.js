/*
show dbs
show collections

use local //trocar banco

db.createCollection("banco") //cria collection banco

db.banco.find() //busca todos da collection

db.banco.insert(  //Inserção de objeto formato JSON no banco.
    {
        codigo: 0001,
        nome: "Alfa",
        agencia: [
            {
                nome: "Web"
            },
            {
                nome: "Web1"
            }
        ] 
    }
)

db.banco.find(); - busca tudo do banco


//Para inserir elementos em banco adiciona-se o objeto JSON em uma variável e insere-se a variável no banco. Ex:
abacaxi = {
        codigo: 0001,
        nome: "Alfa",
        agencia: [
            {
                nome: "Web"
            },
            {
                nome: "Web1"
            }
        ] 
    }
    
 db.banco.insert(abacaxi)
 
 
 db.banco.find().pretty() = faz aparacer identado
 
 ***************************************************
 ATIVIDADE:
 
 Pegar o arquivo de criação dos bancos e agências como endereços do SQL e criar todos em formato NoSQL sem relacionamentos.
 
 ***************************************************
 
 db.banco.find(); -- buscar todos os dados do banco 
  db.banco.find({ nome: "Beta" }); - //busca por chave
 
 
 db.banco.find({nome:{$regex: /A/ }});
 
 db.banco.find({codigo: {$eq: 9}}); //busca por igual (=)
 
 db.banco.find({codigo: {$ne: "001"}}); //not equal (<>)
 
 db.banco.find({codigo: {$gt: 21}}); //greater than   (lt = menor que)
 
 db.banco.find({"agencia.nome": "Web"}); //"Entrando nos objetos da collection" - retorna o objeto inteiro.
 
 db.banco.find({"agencia.conta.saldo" : {$gt: 1000000}}); //comparando valor da chave saldo, da chave conta, da chave agencia com o valor colocado.  - lte - lesser than-egual <=
 
 db.banco.find().sort({"agencia.conta.saldo": 1}); //Ordena o resultado => 1 (ordem crescente); -1 (ordem decrescente)
 
 db.banco.update({nome: "Alfa"}, {$set: {nome: "Alfa 2"}}) //1 - filtro de busca; 2 - valor novo
 
 $unset - deletar o campo todo colocado.
 $inc - incrementa um valor específico
 $mul - multiplica o valor do campo
 $rename - renomeaia o campo
 $delete - One e Many

*/
db.banco.deleteOne({ nome: "Alfa" }); //deleta o primeiro valor encontrado

_id: ObjectId("")

