use local;

db.createCollection("pais");
db.pais.insert({
    nome: "Brasil"
});

db.pais.find();

db.createCollection("estado");
db.estado.insert({
    pais: ObjectId("60bfb1d3bb7c2747eb6a1fde"),
    nome: "Rio Grande Do Sul"
});

db.estado.find();

db.createCollection("cidade");
db.cidade.insert({
    estado: ObjectId("60bfb278bb7c2747eb6a1fdf"),
    nome: "Porto Alegre"
});

db.cidade.find();

db.createCollection("bairro");
db.bairro.insert({
    estado: ObjectId("60bfb278bb7c2747eb6a1fdf"),
    cidade: ObjectId("60bfb2cabb7c2747eb6a1fe0"),
    nome: "Centro"
});

db.bairro.find();

db.createCollection("endereco");
db.endereco.insert(
    {
        logradouro: "Rua Testada",
        numero: "187",
        complemento: "Ap 401",
        bairro: ObjectId("60bfb327bb7c2747eb6a1fe1"),
        cidade: ObjectId("60bfb2cabb7c2747eb6a1fe0"),
        estado: ObjectId("60bfb278bb7c2747eb6a1fdf"),
        pais: ObjectId("60bfb1d3bb7c2747eb6a1fde")
    }
);

db.endereco.find();

db.createCollection("cliente");
db.cliente.insert({
    cpf: "234.543.200-89",
    nome: "João",
    estadoCivil: "Solteiro",
    dataNascimento: "22/11/1978",
    endereco: ObjectId("60bfb456bb7c2747eb6a1fe2")
});

db.cliente.find();

db.createCollection("movimentacao");
db.movimentacao.insert({
    tipo: "debito",
    valor: 5540.0
});

db.movimentacao.find();

db.createCollection("gerente");
db.gerente.insert({
    cpf: "234.543.200-89",
    nome: "Jessica",
    estadoCivil: "Casada",
    dataNascimento: "11/05/1991",
    codigoFuncionario: "0001",
    tipoGerente: "GC",
    endereco: ObjectId("60bfb456bb7c2747eb6a1fe2")
});

db.gerente.find();
    
db.createCollection("conta");
db.conta.insert({
    codigo: "0001",
    tipo: "PJ",
    saldo: 500000,
    gerente: [
        ObjectId("60bfb619bb7c2747eb6a1fe6")
    ],
    cliente: [
        ObjectId("60bfb4cbbb7c2747eb6a1fe3"),
        ObjectId("60bfb4cbbb7c2747eb6a1fe3"),
        ObjectId("60bfb4cbbb7c2747eb6a1fe3")
    ],
    movimentacao: [
        ObjectId("60bfb4e7bb7c2747eb6a1fe4")
    ]
    
});

db.conta.find();

db.createCollection("consolidacao");
db.consolidacao.insert({
    saldoAtual: 54030245,
    saques: 334,
    depositos: 45,
    numeroCorrentistas: 10
});

db.consolidacao.find();

db.createCollection("agencia");
db.agencia.insert([
    {
        codigo: "0001",
        nome: "Web",
        endereco: ObjectId("60bfb456bb7c2747eb6a1fe2"),
        consolidacao: [
            ObjectId("60bfb808bb7c2747eb6a1fea"),
            ObjectId("60bfb808bb7c2747eb6a1fea"),
            ObjectId("60bfb808bb7c2747eb6a1fea")
        ],
        conta: [
            ObjectId("60bfb6f7bb7c2747eb6a1fe7"),
            ObjectId("60bfb72ebb7c2747eb6a1fe8"),
            ObjectId("60bfb734bb7c2747eb6a1fe9"),
        ]
    },
    {
        codigo: "0002",
        nome: "California",
        endereco: ObjectId("60bfb456bb7c2747eb6a1fe2"),
        consolidacao: [
            ObjectId("60bfb808bb7c2747eb6a1fea"),
            ObjectId("60bfb808bb7c2747eb6a1fea"),
            ObjectId("60bfb808bb7c2747eb6a1fea")
        ],
        conta: [
            ObjectId("60bfb6f7bb7c2747eb6a1fe7"),
            ObjectId("60bfb72ebb7c2747eb6a1fe8"),
            ObjectId("60bfb734bb7c2747eb6a1fe9"),
        ]
        
    },
    {
        codigo: "0101",
        nome: "Londres",
        endereco: ObjectId("60bfb456bb7c2747eb6a1fe2"),
        consolidacao: [
            ObjectId("60bfb808bb7c2747eb6a1fea"),
            ObjectId("60bfb808bb7c2747eb6a1fea"),
            ObjectId("60bfb808bb7c2747eb6a1fea")
        ],
        conta: [
            ObjectId("60bfb6f7bb7c2747eb6a1fe7"),
            ObjectId("60bfb72ebb7c2747eb6a1fe8"),
            ObjectId("60bfb734bb7c2747eb6a1fe9"),
        ]
        
    },
    {
        codigo: "0001",
        nome: "Web",
        endereco: ObjectId("60bfb456bb7c2747eb6a1fe2"),
        consolidacao: [
            ObjectId("60bfb808bb7c2747eb6a1fea"),
            ObjectId("60bfb808bb7c2747eb6a1fea"),
            ObjectId("60bfb808bb7c2747eb6a1fea")
        ],
        conta: [
            ObjectId("60bfb6f7bb7c2747eb6a1fe7"),
            ObjectId("60bfb72ebb7c2747eb6a1fe8"),
            ObjectId("60bfb734bb7c2747eb6a1fe9")
        ]
        
    },
    {
        codigo: "0001",
        nome: "Web",
        endereco: ObjectId("60bfb456bb7c2747eb6a1fe2"),
        consolidacao: [
            ObjectId("60bfb808bb7c2747eb6a1fea"),
            ObjectId("60bfb808bb7c2747eb6a1fea"),
            ObjectId("60bfb808bb7c2747eb6a1fea")
        ],
        conta: [
            ObjectId("60bfb6f7bb7c2747eb6a1fe7"),
            ObjectId("60bfb72ebb7c2747eb6a1fe8"),
            ObjectId("60bfb734bb7c2747eb6a1fe9"),
        ]
        
    },
    {
        codigo: "8761",
        nome: "Itu",
        endereco: ObjectId("60bfb456bb7c2747eb6a1fe2"),
        consolidacao: [
            ObjectId("60bfb808bb7c2747eb6a1fea"),
            ObjectId("60bfb808bb7c2747eb6a1fea"),
            ObjectId("60bfb808bb7c2747eb6a1fea")
        ],
        conta: [
            ObjectId("60bfb6f7bb7c2747eb6a1fe7"),
            ObjectId("60bfb72ebb7c2747eb6a1fe8"),
            ObjectId("60bfb734bb7c2747eb6a1fe9"),
        ]
        
    },
    {
        codigo: "4567",
        nome: "Hermana",
        endereco: ObjectId("60bfb456bb7c2747eb6a1fe2"),
        consolidacao: [
            ObjectId("60bfb808bb7c2747eb6a1fea"),
            ObjectId("60bfb808bb7c2747eb6a1fea"),
            ObjectId("60bfb808bb7c2747eb6a1fea")
        ],
        conta: [
            ObjectId("60bfb6f7bb7c2747eb6a1fe7"),
            ObjectId("60bfb72ebb7c2747eb6a1fe8"),
            ObjectId("60bfb734bb7c2747eb6a1fe9"),
        ]
        
    }
    
]);

db.agencia.find();

db.createCollection("banco");
db.banco.insert(
    [
        {
            codigo: "001",
            nome: "Alfa",
            agencia: [
                ObjectId("60bfba82bb7c2747eb6a1feb"),
                ObjectId("60bfba82bb7c2747eb6a1fec"),
                ObjectId("60bfba82bb7c2747eb6a1fed")
            ]
        },
        {
            codigo: "241",
            nome: "Beta",
            agencia: [
                ObjectId("60bfba82bb7c2747eb6a1fee")                
            ]
        },
        {
            codigo: "307",
            nome: "Omega",
            agencia: [
                ObjectId("60bfba82bb7c2747eb6a1fef"),
                ObjectId("60bfba82bb7c2747eb6a1ff0"),
                ObjectId("60bfba82bb7c2747eb6a1ff1")
            ]
        }
    ]
);

db.banco.find();