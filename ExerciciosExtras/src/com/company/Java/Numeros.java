package com.company.Java;

public class Numeros {

    private double[] numeros;

    public Numeros (double[] numeros){
        this.numeros = numeros;
    }

    private int getTamanho(){
        return this.numeros.length;
    }

    private double calcularMedia(double numero1, double numero2){
        return (numero1 + numero2) / 2;
    }

    public double[] calcularMediaSeguinte(){
        int tamanho = this.getTamanho();
        double[] medias = new double[tamanho - 1];
        for (int i = 0; i < tamanho - 1; i++){
                medias[i] = calcularMedia(this.numeros[i], this.numeros[i + 1]);
        }
        return medias;
    }

    public double[] getNumeros() {
        return numeros;
    }

    public void setNumeros(double[] numeros) {
        this.numeros = numeros;
    }
}
