package com.company.Java;

public class Item {

    private int quantidade;
    private String descricao;

    public Item (int quantidade, String descricao){
        this.quantidade = quantidade;
        this.descricao = descricao;
    }

    public void aumentarQuantidade (int quantidade){
        this.quantidade += quantidade;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
