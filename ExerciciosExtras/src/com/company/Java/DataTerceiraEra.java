package com.company.Java;

public class DataTerceiraEra {

    private final int dia;
    private final int mes;
    private final int ano;

    public DataTerceiraEra (int dia, int mes, int ano){
        this.dia = dia;
        this.mes = mes;
        this.ano = ano;
    }

    public boolean bissexto() {
        int ano = this.getAno();
        return (ano % 400 == 0) || (ano % 4 == 0 && ano % 100 != 0);
    }

    public int getDia() {
        return dia;
    }

    public int getMes() {
        return mes;
    }

    public int getAno() {
        return ano;
    }
}
