package com.company.Java;
public class CestoDeLembas {

    private int numeroLembas;

    public CestoDeLembas (int numeroLembas){
        this.numeroLembas = numeroLembas;
    }

    public boolean podeDividirEmPares(){
        int quantidade = this.numeroLembas;
        return quantidade < 100 && quantidade > 2 && quantidade % 2 == 0;
    }

    public int getNumeroLembas() {
        return numeroLembas;
    }

    public void setNumeroLembas(int numeroLembas) {
        this.numeroLembas = numeroLembas;
    }
}
