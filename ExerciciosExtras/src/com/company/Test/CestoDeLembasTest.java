package com.company.Test;

import static org.junit.Assert.*;
import org.junit.Test;


import com.company.Java.CestoDeLembas;


public class CestoDeLembasTest {

    @Test
    public void podeDividirEmPares(){
        CestoDeLembas cesto = new CestoDeLembas(101);
        CestoDeLembas cesto1 = new CestoDeLembas(15);
        CestoDeLembas cesto2 = new CestoDeLembas(20);
        CestoDeLembas cesto3 = new CestoDeLembas(32);
        CestoDeLembas cesto4 = new CestoDeLembas(-1);
        CestoDeLembas cesto5 = new CestoDeLembas(2);

        assertFalse(cesto.podeDividirEmPares());
        assertFalse(cesto1.podeDividirEmPares());
        assertTrue(cesto2.podeDividirEmPares());
        assertTrue(cesto3.podeDividirEmPares());
        assertFalse(cesto4.podeDividirEmPares());
        assertFalse(cesto5.podeDividirEmPares());

    }
}
