package com.company.Test;

import static org.junit.Assert.*;

import com.company.Java.DataTerceiraEra;
import org.junit.Test;

public class DataTerceiraEraTest {

    @Test
    public void adicionarDataTerceiraEra(){
        DataTerceiraEra data = new DataTerceiraEra(1, 10, 3019);
        assertEquals(data.getDia(), 1);
        assertEquals(data.getMes(), 10);
        assertEquals(data.getAno(), 3019);
    }

    @Test
    public void EhBissexto(){
        DataTerceiraEra dataBS = new DataTerceiraEra(1, 10, 2020);
        DataTerceiraEra dataNBS = new DataTerceiraEra(1, 10, 2017);
        assertTrue(dataBS.bissexto());
        assertFalse(dataNBS.bissexto());
    }
}
