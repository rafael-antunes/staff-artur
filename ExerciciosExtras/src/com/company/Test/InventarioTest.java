package com.company.Test;

import com.company.Java.Inventario;
import com.company.Java.Item;


import static org.junit.Assert.*;
import org.junit.Test;

public class InventarioTest {

    @Test
    public void unirInventarios(){
        Item cigarro = new Item(20, "Cigarro");
        Item isqueiro = new Item(1, "Isqueiro");
        Item camiseta = new Item(5, "Camiseta");
        Item calca = new Item(2, "Calça");
        Item bermuda = new Item(3, "Bermuda");
        Item oculos = new Item(1, "Óculos");

        Inventario inventario1 = new Inventario();
        inventario1.adicionar(cigarro);
        inventario1.adicionar(isqueiro);
        inventario1.adicionar(camiseta);

        Inventario inventario2 = new Inventario();
        inventario2.adicionar(cigarro);
        inventario2.adicionar(isqueiro);
        inventario2.adicionar(calca);
        inventario2.adicionar(bermuda);
        inventario2.adicionar(oculos);

        Inventario unido = inventario1.unir(inventario2);

        assertEquals(unido.getItens().size(), 6);
        assertEquals(unido.getItens().get(0).getQuantidade(), 40);
        assertEquals(unido.getItens().get(1).getQuantidade(), 2);
        assertEquals(unido.getItens().get(5).getDescricao(), "Óculos");
    }

    @Test
    public void diferenciarInventarios(){
        Item cigarro = new Item(20, "Cigarro");
        Item isqueiro = new Item(1, "Isqueiro");
        Item camiseta = new Item(5, "Camiseta");
        Item calca = new Item(2, "Calça");
        Item bermuda = new Item(3, "Bermuda");
        Item oculos = new Item(1, "Óculos");

        Inventario inventario1 = new Inventario();
        inventario1.adicionar(cigarro);
        inventario1.adicionar(isqueiro);
        inventario1.adicionar(camiseta);

        Inventario inventario2 = new Inventario();
        inventario2.adicionar(cigarro);
        inventario2.adicionar(isqueiro);
        inventario2.adicionar(calca);
        inventario2.adicionar(bermuda);
        inventario2.adicionar(oculos);

        Inventario diferenciado = inventario1.diferenciar(inventario2);

        assertEquals(diferenciado.getItens().size(), 1);
        assertEquals(diferenciado.getItens().get(0).getQuantidade(), 5);
        assertEquals(diferenciado.getItens().get(0).getDescricao(), "Camiseta");

    }

    @Test
    public void cruzarInventarios(){
        Item cigarro = new Item(20, "Cigarro");
        Item isqueiro = new Item(1, "Isqueiro");
        Item camiseta = new Item(5, "Camiseta");
        Item calca = new Item(2, "Calça");
        Item bermuda = new Item(3, "Bermuda");
        Item oculos = new Item(1, "Óculos");

        Inventario inventario1 = new Inventario();
        inventario1.adicionar(cigarro);
        inventario1.adicionar(isqueiro);
        inventario1.adicionar(camiseta);

        Inventario inventario2 = new Inventario();
        inventario2.adicionar(cigarro);
        inventario2.adicionar(isqueiro);
        inventario2.adicionar(calca);
        inventario2.adicionar(bermuda);
        inventario2.adicionar(oculos);

        Inventario cruzado = inventario1.cruzar(inventario2);

        assertEquals(cruzado.getItens().size(), 2);
        assertEquals(cruzado.getItens().get(0).getQuantidade(), 40);
        assertEquals(cruzado.getItens().get(1).getQuantidade(), 2);
    }

    @Test
    public void intersecInventarios(){
        Item cigarro = new Item(20, "Cigarro");
        Item isqueiro = new Item(1, "Isqueiro");
        Item camiseta = new Item(5, "Camiseta");
        Item calca = new Item(2, "Calça");
        Item bermuda = new Item(3, "Bermuda");
        Item oculos = new Item(1, "Óculos");

        Inventario inventario1 = new Inventario();
        inventario1.adicionar(cigarro);
        inventario1.adicionar(isqueiro);
        inventario1.adicionar(camiseta);

        Inventario inventario2 = new Inventario();
        inventario2.adicionar(cigarro);
        inventario2.adicionar(isqueiro);
        inventario2.adicionar(calca);
        inventario2.adicionar(bermuda);
        inventario2.adicionar(oculos);

        Inventario resultado = inventario1.intersec(inventario2);
        assertEquals(resultado.getItens().size(), 2);
        assertEquals(resultado.getItens().get(0).getDescricao(), "Cigarro" );
        assertEquals(resultado.getItens().get(1).getDescricao(), "Isqueiro" );
    }


}
