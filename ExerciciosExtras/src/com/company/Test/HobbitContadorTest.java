package com.company.Test;

import static org.junit.Assert.*;

import com.company.Java.HobbitContador;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class HobbitContadorTest {

    @Test
    public void calcularDiferenca(){
        ArrayList<ArrayList<Integer>> arrayDePares = new ArrayList<>();
        arrayDePares.add(new ArrayList<>(Arrays.asList(15, 18)));
        arrayDePares.add(new ArrayList<>(Arrays.asList(4, 5)));
        arrayDePares.add(new ArrayList<>(Arrays.asList(12, 60)));

        HobbitContador contador = new HobbitContador();
        int resultado = contador.calcularDiferenca(arrayDePares);

        assertEquals(resultado, 840);

    }
}
